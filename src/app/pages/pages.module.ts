import { VSyncContactsPageComponent } from './vsync-contacts-page/vsync-contacts-page.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ParentalLogsPageComponent } from './parental-logs-page/parental-logs-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { TutorialCardsComponentModule } from './../components/tutorial-cards/tutorial-cards-component.module';
import { CommonModule, Location } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SqliteService } from '../services/sqlite.service';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { ForgetPasswordPageComponent } from './forget-password-page/forget-password-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { PagesRoutingModule } from './pages-routing.module';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { EmptyviewComponent } from '../components/emptyview/emptyview.component';
import { VContactsPageComponentModule } from './vcontacts-page/vcontacts-page.component.module';
import { NewPassPageComponent } from './new-pass-page/new-pass-page.component';
import { ContactSearchListComponent } from '../components/contact-search-list/contact-search-list.component';
import { ContactItemComponent } from '../components/contact-item/contact-item.component';
import { GmapPageComponent } from './gmap-page/gmap-page.component';
import { PassformComponent } from './new-pass-page/passform/passform.component';
import { PasscontactsComponent } from './new-pass-page/passcontacts/passcontacts.component';
import { PassendpageComponent } from './new-pass-page/passendpage/passendpage.component';
import { OtherEventsPageComponent } from './other-events-page/other-events-page.component';
import { ManageVehiclePageComponent } from './manage-vehicle-page/manage-vehicle-page.component';
import { CreateVehiclePageComponent } from './create-vehicle-page/create-vehicle-page.component';
import { ComponentsNotificationItemComponent } from '../components/components-notification-item/components-notification-item.component';
import { NotificationsPageComponent } from './notifications-page/notifications-page.component';
import { ParentalNotificationItemComponent } from '../components/parental-notification-item/parental-notification-item.component';
import { CodeVerificationPageComponent } from './code-verification-page/code-verification-page.component';
import { RequestBecomeResidentPageComponent } from './request-become-resident-page/request-become-resident-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { WelcomescreenComponent } from './register-page/welcomescreen/welcomescreen.component';
import { FirstscreenComponent } from './register-page/firstscreen/firstscreen.component';
import { SecondscreenComponent } from './register-page/secondscreen/secondscreen.component';
import { ThirdscreenComponent } from './register-page/thirdscreen/thirdscreen.component';
import { SuccessscreenComponent } from './register-page/successscreen/successscreen.component';
import { CountryCodePageComponent } from './country-code-page/country-code-page.component';
import { CropperComponent } from '../components/cropper/cropper.component';
import { MypassesPageComponentModule } from './mypasses-page/mypasses-page.component.module';
import { EmptyviewComponentModule } from '../components/emptyview/emptyview.component.module';
import { MyfamilymembersPageComponent } from './myfamilymembers-page/myfamilymembers-page.component';
import { ParentalControlsPageComponent } from './parental-controls-page/parental-controls-page.component';
import { MypassesdetailPageComponent } from './mypassesdetail-page/mypassesdetail-page.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { ChoiceOptionsComponent } from './request-apass-page/choice-options/choice-options.component';
import { CommentOptionsComponent } from './request-apass-page/comment-options/comment-options.component';
import { RequestAPassPageComponent } from './request-apass-page/request-apass-page.component';
import { RequestAPassPageComponentModule } from './request-apass-page/request-apass-page.component.module';
import { CreateFamilyPageComponent } from './create-family-page/create-family-page.component';
import { VSyncContactsStatusPageComponent } from './vsync-contacts-status-page/vsync-contacts-status-page.component';

@NgModule({
    declarations: [
        SplashPageComponent,
        SignupComponent,
        ForgetPasswordPageComponent,
        
        DashboardPageComponent,

        // WelcomePageComponent,
        // RegisterPageComponent,
        LoginPageComponent,
        // ForgetPasswordPageComponent,
        TutorialPageComponent,
        
        ContactItemComponent,
        ContactSearchListComponent,
        GmapPageComponent,
        NewPassPageComponent,
        PassformComponent,
        PasscontactsComponent,
        OtherEventsPageComponent,
        PassendpageComponent,
        ManageVehiclePageComponent,
        CreateVehiclePageComponent,
        NotificationsPageComponent,
        ParentalNotificationItemComponent,
        ParentalLogsPageComponent,
        ComponentsNotificationItemComponent,
        CodeVerificationPageComponent,
        RequestBecomeResidentPageComponent,
        RegisterPageComponent,
        WelcomescreenComponent,
        FirstscreenComponent,
        SecondscreenComponent,
        ThirdscreenComponent,
        SuccessscreenComponent,
        CountryCodePageComponent,
        CropperComponent,
        MyfamilymembersPageComponent,
        ParentalControlsPageComponent,
        MypassesdetailPageComponent,
        CreateFamilyPageComponent,
        VSyncContactsPageComponent,
        VSyncContactsStatusPageComponent
        
        

        // DashboardPageComponent,
    ],
    imports: [
        CommonModule,
        PagesRoutingModule,
        // NgxPubSubModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        TutorialCardsComponentModule,
        VContactsPageComponentModule,
        MypassesPageComponentModule,
        RequestAPassPageComponentModule,
        ImageCropperModule,
        EmptyviewComponentModule,
        NgxQRCodeModule

        // TutorialCardsComponentModule
        // NotificationItemComponentModule,
        // ParentalNotificationItemComponentModule
    ], 
    exports: [
        
    ],
    providers: [
        SqliteService,
        Location,
    ]

})
export class PagesModule{}