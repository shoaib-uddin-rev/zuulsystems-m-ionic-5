import { Component, OnInit, Injector, ViewChild, Input, AfterViewInit } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { VehicleServiceService } from 'src/app/services/vehicle-service.service';
import { BasePage } from '../base-page/base-page';
import { CreateVehiclePageComponent } from '../create-vehicle-page/create-vehicle-page.component';

@Component({
  selector: 'app-manage-vehicle-page',
  templateUrl: './manage-vehicle-page.component.html',
  styleUrls: ['./manage-vehicle-page.component.scss'],
})
export class ManageVehiclePageComponent extends BasePage implements OnInit, AfterViewInit {

  @ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
  search_value;
  @Input() search_on = false;
  @Input() from_new_pass = false;
  selected_item_id = -1;
  loading = false;
  plist: any[] = [];
  _plist: any[] = [];

  constructor(injector: Injector, public vehicleService: VehicleServiceService) { 
    super(injector);
    
  }
  ngAfterViewInit(): void {
    this.getVehicleList();
  }

  ngOnInit() {}

  async getVehicleList(){
    this.loading = true;
    this.plist = await this.vehicleService.getVehiclesList();
    this._plist = [...this.plist];
    this.loading = false;
    // console.log(this.plist);
  }

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event){
    let val = this.search_value;
    this.plist = this._plist.filter( item => {
      return item.licence_plate.toLowerCase().indexOf(val.toLowerCase()) > -1
      || item.make.toLowerCase().indexOf(val.toLowerCase()) > -1
      || item.model.toLowerCase().indexOf(val.toLowerCase()) > -1
      || item.year.toLowerCase().indexOf(val.toLowerCase()) > -1
      || item.color.toLowerCase().indexOf(val.toLowerCase()) > -1
    })
    // this.storedcontacts.getMyContacts(val, true, 0, false).then( v => {
    //   console.log(v);
    // });

  }

  resetSearch(){
    this.search_value = null;
    // this.storedcontacts.getMyContacts(null, true, 0, false)
  }

  async doRefresh($event){
    this._plist = await this.vehicleService.getVehiclesList();
    this.plist = [...this._plist];
    $event.target.complete();
  }

  loadMoreContacts($event){
    // this.storedcontacts.getNetworkContacts(this.search_value, 0, true).then( v => {
    //   $event.complete();
    // });
  }

  plusHeaderButton() {
    this.editVehicle(null);
  }

  async editVehicle(item){

    const _data = await this.modals.present(CreateVehiclePageComponent, {item});
    const data = _data.data;
    
    if(data['data'] != 'A'){
      var vehicle = data;
      vehicle.new = true;
      vehicle.image = vehicle.ImageUrl;
      const _index = this._plist.findIndex(x => x.id == vehicle.id);
      if(_index != -1){
        this._plist[_index] = vehicle;
      }else{
        this._plist.unshift(vehicle)
      }

      this.plist = [...this._plist];

    }

  }

  async deleteVehicle(item){
    await this.vehicleService.deleteVehicle(item['id'])
    this.getVehicleList();
  }

  async setVehicleToPass(item){
    this.selected_item_id = item.id;
    await this.vehicleService.setDefaultVehicle(item['id']);

    if(this.from_new_pass){
      // check if need to dismiss modally
      this.closeModal({ data: item});
    }


  }

  async presentPopover($event, item) {

    $event.stopPropagation();
    const _data = await this.popover.present($event, {
      pid: item,
      flag: 'E'
    });
    const data = _data['data'];
    console.log(data);

    if (data == null) {
      return;
    }
    var item = data['pid'];
    switch (data['param']) {
      case 'S':
        this.setVehicleToPass(item)
        break;
      case 'E':
        this.editVehicle(item);
        break;
      case 'D':
        this.deleteVehicle(item)
        break;
    }
  }

  closeModal(res){
    this.modals.dismiss(res);
  }

}
