import { StoredContactsService } from './../../services/stored-contacts.service';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { CreateFamilyPageComponent } from '../create-family-page/create-family-page.component';

@Component({
  selector: 'app-request-apass-page',
  templateUrl: './request-apass-page.component.html',
  styleUrls: ['./request-apass-page.component.scss'],
})
export class RequestAPassPageComponent extends BasePage implements OnInit {

  @ViewChild('slides', {static: true}) slides: IonSlides;
  selected_contacts: any = [];
  comments: string = "";

  heading1 = 'Pass request has been sent. You will be notified once a pass has been issued.';
  Start = 'Finish'

  constructor(injector: Injector, public storedcontacts: StoredContactsService) { 
    super(injector);

    this.events.subscribe("requestpass:selectfromexicting", this.selectFromExicting.bind(this))
    this.events.subscribe("requestpass:goToNext", this.goToNext.bind(this))
    this.events.subscribe("requestpass:setComments", this.setComments.bind(this));
    this.events.subscribe("requestpass:close", this.goBack.bind(this));

  }

  ngOnInit() {}

  goBack(){
    this.modals.dismiss();
  }

  setComments(comments): Promise<any>{
    return new Promise( resolve => {
      this.comments = comments;
      resolve();
    })
    
  }



  async selectFromExicting(bool){
    this.slides.lockSwipes(false);
    if(bool){
      // select a contact from list and get back
      // this.slides.slideTo(1, 500);

      // ask API to load users that are in my contact list that can send passes

      let contact_ids = await this.getResidentsListToRequestPass();
      console.log(contact_ids);
      if(contact_ids.length <= 0){
        this.utility.presentSuccessToast("No Residential Contacts Found");
        this.utility.hideLoader();
      }else{

        console.log("peep");
        let imported_list = await this.storedcontacts.returnContactSelection(true, true, true, contact_ids);
        let contacts = await this.storedcontacts.getContactsByArrayOfIds(imported_list);

        console.log(imported_list, contacts);

        if(contacts.length){
          this.selected_contacts = [...contacts];
          this.goToPre(1);
        }else{
          this.utility.presentSuccessToast("No Residential Contacts Found");
        }

      }




    }else{
      this.editFamily()
    }

    this.slides.lockSwipes(true);
  }

  getResidentsListToRequestPass(): Promise<any[]>{
    return new Promise( resolve => {

      this.network.getResidentsListToRequestPass().then( v => {
        console.log(v);
        resolve(v["contact_ids"]);

      }, err => {
        console.error(err);
        resolve([]);
      })

    })
  }


  async editFamily(){

    // CreateGroupPage as modal
    
    const _data = await this.modals.present(CreateFamilyPageComponent, {user_id: false, show_relation: false, isFromRequestPassScreen: true});
    const data = _data.data;

    if(data['data'] != 'A'){
      this.selected_contacts = [];
      this.selected_contacts.push(data['contact']);
      this.goToPre(1);
    }

  }

  goToPre(num) {
      this.slides.lockSwipes(false);
      this.slides.slideTo(num, 500);
      this.slides.lockSwipes(true);
  }

  goToNext(num) {

    if(!this.comments || this.comments == ""){
      return;
    }


    let id = this.users._user.id;
    // console.log(id);

    var self = this;
    var conts = [];
    this.selected_contacts.forEach(element => {

      //var prett = this.phone_contacts.filter( x => x['its_user'] != null);
      // console.log(element);
      var t = element;

          var f = {};
          f['requested_user_id'] = t["user_id"];
          f['sent_user_id'] = id;
          f['display_name'] = t.display_name;
          f['display_email'] = t.email;
          f['description'] = this.comments;
          f['phone_number'] = t.phone_number;


          conts.push(f);

    });

    // console.log(conts)

    this.network.sendRequestForAPass(conts).then((response: any) =>{

      // console.log(response);

      this.slides.lockSwipes(false);
      this.slides.slideTo(num, 500);
      this.slides.lockSwipes(true);

    }, err =>{})


  }

  finishregister(data){
    this.modals.dismiss();
  }


}
