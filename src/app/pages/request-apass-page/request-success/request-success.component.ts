import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-request-success',
  templateUrl: './request-success.component.html',
  styleUrls: ['./request-success.component.scss'],
})
export class RequestSuccessComponent extends BasePage implements OnInit {

  @Input() heading1;
  @Input() Start;
  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  dashboardopen(){
    this.modals.dismiss().then( () => {
      this.events.publish("requestpass:finish");
    })
    
  }

}
