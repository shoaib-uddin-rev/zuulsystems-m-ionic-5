import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-choice-options',
  templateUrl: './choice-options.component.html',
  styleUrls: ['./choice-options.component.scss'],
})
export class ChoiceOptionsComponent extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
    
  }

  ngOnInit() {}

  selectFromExicting(flag){
    this.events.publish("requestpass:selectfromexicting", flag);
  }

  goBack(){
    this.events.publish("requestpass:close");
  }

}
