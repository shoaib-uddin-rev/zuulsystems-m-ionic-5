import { ParentalLogsPageComponent } from './../parental-logs-page/parental-logs-page.component';
import { Component, Injector, OnInit, AfterViewInit } from '@angular/core';
import { ParentalNotificationItemComponentModule } from 'src/app/components/parental-notification-item/parental-notification-item.module';
import { AnnouncementPageComponent } from '../announcement-page/announcement-page.component';
import { BasePage } from '../base-page/base-page';
import { ManageVehiclePageComponent } from '../manage-vehicle-page/manage-vehicle-page.component';
import { MypassesPageComponent } from '../mypasses-page/mypasses-page.component';
import { MypassesdetailPageComponent } from '../mypassesdetail-page/mypassesdetail-page.component';
import { NewPassPageComponent } from '../new-pass-page/new-pass-page.component';
import { NotificationsPageComponent } from '../notifications-page/notifications-page.component';
import { RequestAPassPageComponent } from '../request-apass-page/request-apass-page.component';
import { UpdatePasswordPageComponent } from '../update-password-page/update-password-page.component';
import { CodeVerificationPageComponent } from '../code-verification-page/code-verification-page.component';
import { RegisterPageComponent } from '../register-page/register-page.component';
import { NavigationEnd, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent extends BasePage implements OnInit {

  avatar = this.users.avatar;
  canBeResident: boolean = false;
  isUserLoaded: boolean = false;
  user: any;
  plist: any[] = [];
  storedData;
  offset = 0;
  rnotif: boolean = false;
  contact_count = 0;
  getNotified: boolean = false;
  isEmailVerificationPending: boolean = false;
  suspandAccount: boolean = false;
  disableCreatePass: boolean = true;
  notification_number = -1;

  constructor(injector: Injector) {
    super(injector);

    this.initializeView();
    this.events.unsubscribe('user:shownotification');
    this.events.subscribe('user:shownotification', this.notificationReceived.bind(this));
    this.events.unsubscribe('user:redirectToCreatePass');
    this.events.unsubscribe('dashboard:carselection');
    this.events.subscribe('dashboard:carselection', this.openVehicleSelection.bind(this) );
    this.events.subscribe('user:redirectToCreatePass', this.mypassThenNewPass.bind(this));
    this.events.subscribe('dashboard:updateprofile', this.openRegistrationPage.bind(this));
    this.events.subscribe('dashboard:initialize', this.initializeView.bind(this));

  }

  ngOnInit() {

  }

  AfterViewInit(){
    
  }

  openRegistrationPage(){
    this.menuCtrl.close();
    this.modals.present(RegisterPageComponent, { new: false, user: this.user })
  }

  async initializeView(){

    var showelcome = this.nav.getQueryParams().showelcome;
    var self = this;
    this.user = await this.sqlite.getActiveUser();
    this.users._user = this.user;

    this.canBeResident = ( parseInt(this.user["can_user_become_resident"]) == 1 );
    this.suspandAccount = (parseInt(this.user['suspand']) == 1) ? true : false;
    this.disableCreatePass = !(parseInt(this.user["can_send_passes"]) == 1);
    this.menuCtrl.enable(true, 'authenticated');
    // sync contact list from global database to local database,
    // if sync already done skip the step

    


    this.getPasses().then(() => {

      if (showelcome == true) {
        this.nav.push('RegistrationPage', { new: true, user: this.user })
      } else
      if (this.user["is_reset_password"] == 1) {
        this.modals.present(UpdatePasswordPageComponent, { user: this.user })
      }
      else if (!this.validateProfile(this.user)) {
        this.nav.push('1/RegistrationPage', { new: true, user: this.user })
      }
      else {
        self.callAnnouncements();
      }
    });
    this.users.isUserEmailPendingVerification(this.user).then( count => {
      this.isEmailVerificationPending = count > 0 ? true : false;
    });
    this.events.publish('user:settokentoserver');

    // this.network.getVendorList().subscribe( async v => {
    //   await this.sqlite.setVendorListInDatabase(v["vendors"]);
    // })

    if(this.suspandAccount == true){
      this.utility.presentFailureToast("Your Account Is Suspended, Please Contact You Admin ");
      return;
    }


    this.events.publish('user:setcontactstodatabase', this.user);
    


  }




  ionViewWillEnter() {
    // 
  }

  ionViewWillLeave() {
    // this.events.unsubscribe('user:shownotification');
    // this.events.unsubscribe('user:redirectToCreatePass');
    // 
  }

  async openVehicleSelection(){
    const data = await this.modals.present(ManageVehiclePageComponent);
  }

  validateProfile(user) {

    // console.log(user);

    if ( user["name"] == "" || !user["name"]
      || user["profile"]["last_name"] == "" || !user["profile"]["last_name"]
      || user["profile"]["date_of_birth"] == "" || !user["profile"]["date_of_birth"]
      || user["phone_number"] == "" || !user["phone_number"]
      || user["dial_code"] == "" || !user["dial_code"]
      || user["email"] == "" || !user["email"]
      || user["profile"]["street_address"] == "" || !user["profile"]["street_address"]
      || user["profile"]["city"] == "" || !user["profile"]["city"]
      || user["profile"]["state"] == "" || !user["profile"]["state"]
      || user["profile"]["zip_code"] == "" || !user["profile"]["zip_code"]
    ) {
      return false;
    }
    return true;
  }


  async notificationReceived(data) {
    console.log(data);
    this.getNotified = true;
    this.utility.presentToast(data["title"]);

    this.offset = 0;
    await this.emptyarray();
    this.storedData = {}
    await this.getPasses( this.storedData, false, true );


  }

  emptyarray(){
    var self = this;
    return new Promise( resolve => {
      self.plist = [];
      resolve();
    })
  }

  presentDuplicateEmailVerification(ev) {
    this.modals.present(CodeVerificationPageComponent, { emv: true });
  }

  async presentPopover(ev) {

    this.getNotified = false;
    this.rnotif = false;

    const data = await this.modals.present(NotificationsPageComponent);
  }

  async presentPopoverParental(){
    this.getNotified = false;
    this.rnotif = false;
    const data = await this.modals.present(ParentalNotificationItemComponentModule);
  }

  callAnnouncements() {

    return new Promise( resolve => {
      this.network.getUnreadAnnouncements().then(async data => {
        // console.log(data);
        var d: any[] = data['list'];
        if (d.length > 0) {
          const data = await this.modals.present(AnnouncementPageComponent, { plist: d });
        }

        resolve();

      }, err => { });
    })

  }

  doRefresh(refresher = null) {

    this.notification_number = -1;

    return new Promise( async resolve => {
      this.offset = 0;
      this.plist = [];
      this.storedData = {};

      await this.getPasses( this.storedData );
      // await this.callAnnouncements();
      if(refresher){
        refresher.target.complete();
      }

      resolve();
    })


  }

  getPasses( data = {}, loader = true, from_notification = false ) {

    return new Promise((resolve) => {

      if ( this.offset == -1 ) {
        resolve();
      }  else {
        data['expired'] = 0;
        data['offset'] = this.offset;
        data['quickpass'] = from_notification == true ? 0 : 1;
        this.network.getUserReceivedPasses(this.user.id, data, loader).then((res: any) => {
          // console.log(data);
          if(from_notification){
            this.plist = res['passes'];
            res['offset'] = 0;
          }else{
            if(this.offset == 0){
              this.plist = res['passes'];
            }else{
              this.plist = this.plist.concat(res['passes']);
            }
          }

          this.offset = res['offset'];
          resolve();
        })
      }

    })
  }

  becomeAResident() {
    this.nav.push('CodeVerificationPage');
  }

  async mycontactbutton() {
    // MyContactPage

    this.nav.push('VContactsPage',{
      animate: true,
      direction: 'forward'
    });
  }

  async mypass() {

    this.nav.push('MypassesPageComponent', {
      animate: true,
      direction: 'forward',
      arc: false
    });




    // const data = await this.modals.present(MypassesPageComponent, { arc: false })
  }

  async mypassThenNewPass(item, ease) {
    // console.log(item, ease);
    const data = await this.modals.present(NewPassPageComponent, { rap: true, group: false, item: item });
  }

  async myNewPass() {
    const data = await this.modals.present(NewPassPageComponent, {item: {}});
  }

  async myArchpass() {
    const data = await this.modals.present(MypassesPageComponent, { arc: true })
  }

  async requestapass() {
    const data = await this.modals.present(RequestAPassPageComponent)
  }

  logout() {
    this.events.publish('user:logout');
  }

  openFamily() {
    this.nav.push('MyfamilymembersPage', {
      animate: true,
      direction: 'forward'
    });
  }

  async passDetails(sel) {
    const data = await this.modals.present(MypassesdetailPageComponent, { item: sel.qrcode_id, flag: "active" });
  }

  async openLogUsers($event) {
    // var r = item;
    var self = this;
    const _data = await this.popover.present($event,{ 
      id: null,
      flag: "SW"
    });

    const data = _data.data;

    if (data == null) {
      return;
    }

    console.log(data);
    let sw_user = data["param"];
    console.log(sw_user);
    this.users.switchUserAccount(sw_user['id']).then( data => {
      if(!data){
        this.nav.push('LoginPage', {sw_user: sw_user});
      }
    })
  }

  async openDoubleBell($event){
    // var r = item;
    var self = this;

    this.getNotified = false;
    this.rnotif = false;
    const _data = await this.popover.present($event,{ 
      pid: null,
      flag: "PCN"
    })

    const data = _data['data']

      // let tr = data;
      console.log(data);
    if (data.data == 'A') {
      return;
    }

    var item = data["pid"];
    switch (data["param"]) {
      case "P":
        await this.modals.present(ParentalLogsPageComponent);
        break;
      case "N":
        await this.modals.present(NotificationsPageComponent);
        break;
    }
  }

  loadMore($event){

    this.getPasses(this.storedData, false).then( v => {
      $event.complete();
    });


  }



}
