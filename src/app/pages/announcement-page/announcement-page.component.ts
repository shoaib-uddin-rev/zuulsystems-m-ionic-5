import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-announcement-page',
  templateUrl: './announcement-page.component.html',
  styleUrls: ['./announcement-page.component.scss'],
})
export class AnnouncementPageComponent extends BasePage implements OnInit {

  @ViewChild('slides', { static: true }) slides: IonSlides;
  getNotified: boolean = false;
  plist: any[] = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() { }

  ngAfterViewInit() {
    // child is set
    this.slides.lockSwipes(true);


  }

  ionViewDidLoad() {
    // // console.log('ionViewDidLoad AnnouncementPage');
  }

  annCheck(item) {
    // // console.log(item)
    var self = this;

    this.network.setReadAnnouncements(item.id).then(res => {

      if (!this.slides.isEnd()) {
        self.slides.lockSwipes(false);
        self.slides.slideNext();
        self.slides.lockSwipes(true);
        // this.removeItem(item);
      } else {
        self.modals.dismiss();
      }
    }, err => { })


  }

  removeItem(item) {
    let index = this.plist.indexOf(item);
    if (index > -1) {
      this.plist.splice(index, 1);
    }
  }

}
