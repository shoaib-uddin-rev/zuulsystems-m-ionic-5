import { SentContactsPageComponent } from './../sent-contacts-page/sent-contacts-page.component';
import { PassGuestLogsPageComponent } from './../pass-guest-logs-page/pass-guest-logs-page.component';
import { Component, OnInit, Injectable, Injector, Input } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { GmapPageComponent } from '../../gmap-page/gmap-page.component';
import { MypassesdetailPageComponent } from '../../mypassesdetail-page/mypassesdetail-page.component';
import { ScanQrHistoryPageComponent } from '../scan-qr-history-page/scan-qr-history-page.component';
import { NewPassPageComponent } from '../../new-pass-page/new-pass-page.component';

@Component({
  selector: 'app-archived-page',
  templateUrl: './archived-page.component.html',
  styleUrls: ['./archived-page.component.scss'],
})
export class ArchivedPageComponent extends BasePage implements OnInit {

  avatar = this.users.avatar;
  storedData;
  plist: any[] = [];
  offset = 0;
  canBeResident: boolean = false;
  isHeadOfFamily: number = 0;
  canSendPasses: number = 0;
  user: any;
  title: string = "My";
  alphalist: any[];
  datelist: any[];
  dcount = 65 * 3;
  searchTerm: string = '';
  seeingOthersPass: boolean = false;
  c_selection = 'archieve';
  user_id;

  constructor(injector: Injector) { 
    super(injector);

    this.alphalist = this.utility.alphalist;
    this.user_id = this.nav.getQueryParams().user_id;
    var display_name = this.nav.getQueryParams().display_name;

    if(display_name){
      this.title = display_name + "'s";
    }

    this.segmentChanged(this.c_selection);
    this.events.subscribe('pass.deleted.id', this.deleteLocallyByPassId.bind(this));
  }

  ngOnInit() {}

  deleteLocallyByPassId(obj){

    var pass_id = obj['pass_id'];
    if(pass_id){
      alert("Here");
      var elementPos = this.plist.map(function (x) { return x.id; }).indexOf(pass_id);
      this.plist.splice(elementPos, 1);
    }
  }

  segmentChanged(choice){

    this.c_selection = choice;
    switch(choice){
      case 'archieve':
        this.doRefresh();
      break
      case 'sent':
        this.doRefresh();
      break
      case 'scanned':
        this.doRefresh();
      break
    }

    this.getPassDates();

  }

  async showGuestLogs(obj, type = 'pass') {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let id = type == 'pass' ? item.pass_id : item.qrcode_id;
    let _data = {pass_id : id, type: type}
    const data = await this.modals.present(PassGuestLogsPageComponent, _data);

  }


  async showCheckbox(obj) {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let _data = {item: item, isHeadOfFamily: this.isHeadOfFamily, allowParentalControl: false};
    const data = await this.modals.present(SentContactsPageComponent, _data);
    let d = data.data;
    if(d['data'] != 'A'){
      item = d;
    }

  }

  async doRefresh(refresher = null){

    if(this.user_id){
      // // console.log(user_id);
      this.canBeResident = false;
      this.seeingOthersPass = true
    }else{
      this.user = await this.sqlite.getActiveUser();
      this.user_id = this.user.id;
      this.canBeResident = this.user['canBeResident'];
      this.isHeadOfFamily = this.user['head_of_family'];
    }


    this.offset = 0;
    this.plist = [];
    this.storedData = {};
    this.getPasses(this.storedData);
    if(refresher){
      refresher.complete();
    }


  }

  closePassTabs(){
    this.events.publish('pass:allTabClose', Date.now());
  }

  getPasses( data = {}, loader = true) {

    return new Promise( resolve => {

      // according to c selection
      var req : Promise<any>;
      data['expired'] = 1;
      data['offset'] = this.offset;

      switch(this.c_selection) {
        case 'archieve':
          req = this.network.getUserReceivedPasses(this.user_id, data, loader);
          break
        case 'sent':
          req = this.network.getUserSentPasses(this.user_id, data, loader);
          break
        case 'scanned':
          req = this.network.getUserScannedPasses(this.user_id, data, loader);
          break
      }

      if(req){
        req.then((res: any) => {

          console.log(res);
          if(this.offset == 0){
            this.plist = res['passes'];
          }else{
            this.plist = this.plist.concat(res['passes']);
          }

          this.offset = res['offset'];
          resolve();

        })
      }else{
        resolve();
      }


    })


  }

  getPassDates(data = {}){

    // according to c selection
    var req: Promise<any>;
    data['expired'] = 1;
    switch(this.c_selection){
      case 'archieve':
        req = this.network.getUserReceivedPassesDates(this.user_id, data)
      break
      case 'sent':
        req = this.network.getUserSentPassesDates(this.user_id, data)
      break
      case 'scanned':
        req = this.network.getUserScannedPassesDates(this.user_id, data);
      break
    }

    if(req){
      req.then((res: any) => {
        this.datelist = res['dates'];
        this.dcount = 65 * this.datelist.length;
      })
    }

  }

  async getDirection(address){

    var addr = address.trim();
    const data = await this.modals.present(GmapPageComponent, {myAddress: addr, newAddress: true, isDirections: true});

  }

  async editDirections(item){

    const _data = await this.modals.present(GmapPageComponent, {myAddress: item.description});

    let data = _data.data;
    if (data['data'] != 'A'){
        item['lat'] = data.lat;
        item['lng'] = data.lng;
        item['description'] = data.address;
        item['id'] = item['pass_id'];

        this.network.updatePassDirections(item).then( res => {}, err => {})
    }

  }

  async passDetails(sel){
    console.log(sel);
    const data = await this.modals.present(MypassesdetailPageComponent, {item: sel.id, flag: "active"});
  }

  filterByDate(a,b)
  {

    this.datelist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });


    this.offset = 0;
    if(a.enable == false){
      this.storedData = {};
    }else{
      this.storedData = { filter_type: 'date', filter_search: a.pass_start_date };

    }

    this.getPasses(this.storedData);

  }

  setFilteredItems(flag){
    var self = this;

    this.offset = 0;
    this.storedData = {search: this.searchTerm.toLowerCase()};
    this.getPasses(this.storedData)

  }

  filterByAlpha(a,b)
  {

    this.alphalist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });

    this.offset = 0;
    if(a.enable == false){
      this.storedData = {};
    }else{
      let param = b == 'sname' ? 'sender_last_name' : b == 'scommunity' ? 'community' : 'event_name';
      this.storedData = {filter_type: param, filter_search: a.alpha.toLowerCase()  };
    }

    this.getPasses(this.storedData);

  }

  async showHistory(obj){

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    console.log("i am mhere", item);
    let data = { items: item.its_scanned_by };
    console.log(data);

    const _data = await this.modals.present(ScanQrHistoryPageComponent, data);

    let d = _data.data;
    if (d['data'] != 'A'){
      item = data;
    }

  }

  async deletePass(item) {

    let flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'This will remove your pass and all associated sent passes along with it ...')

    if(flag){

      var data = {pass_id: item['pass_id']}
      this.network.retractSentPass(this.user.id, data).then((res: any) => {
        this.spliceItem(item)
      }, err => {});

    }

  }

  spliceItem(item){
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
        this.plist.splice(index, 1);
    }
  }

  async editPass(item) {

    console.log(item);
    const data = await this.modals.present(NewPassPageComponent, {item: item, edit: true} );

  }

  loadMore($event){

    this.getPasses(this.storedData, false).then( v => {
      $event.target.complete();
    });


  }



}
