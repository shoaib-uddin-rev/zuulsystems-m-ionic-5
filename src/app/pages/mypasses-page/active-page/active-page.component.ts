import { Component, OnInit, Injector, HostBinding } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { GmapPageComponent } from '../../gmap-page/gmap-page.component';
import { MypassesdetailPageComponent } from '../../mypassesdetail-page/mypassesdetail-page.component';

@Component({
  selector: 'app-active-page',
  templateUrl: './active-page.component.html',
  styleUrls: ['./active-page.component.scss'],
})
export class ActivePageComponent extends BasePage implements OnInit {

  @HostBinding('style.width') width: Number;
  @HostBinding('style.height') height: Number;

  plist: any[] = [];
  storedData;
  offset = 0;
  canBeResident: boolean = false;
  isHeadOfFamily: number = 0;
  user: any;
  title: string = "My";
  alphalist: any[];
  datelist: any[];
  dcount = 160 * 3;
  ccount = 160 * 3;
  communitylist: any[];
  searchTerm: string = '';
  seeingOthersPass: boolean = false;
  user_id
  
  constructor(injector: Injector) { 
    super(injector);

    this.alphalist = this.utility.alphalist;
    this.initialize();

  }

  ngOnInit() {}

  async initialize(){
    await this.doRefresh();
    this.getPassDates();
  }

  cancelSearch($event){
    console.log("cancelsearch");
  }

  async doRefresh(refresher = null){

    return new Promise( async resolve => {
      this.user_id = this.nav.getQueryParams().user_id;
      var display_name = this.nav.getQueryParams().display_name;

      if(display_name){
        this.title = display_name + "'s";
      }

      if(this.user_id){
        // // console.log(user_id);
        this.canBeResident = false;
        this.seeingOthersPass = true

      }else{
        this.user = await this.sqlite.getActiveUser();
        this.user_id = this.user.id;
        this.canBeResident = this.user['can_user_become_resident'];
        this.isHeadOfFamily = this.user['head_of_family'];
      }

      this.offset = 0;
      this.plist = [];
      this.storedData = {};

      await this.getPasses(this.storedData);
      resolve();

      if(refresher){
        refresher.complete();
      }
    })

  }

  getPasses(data = {}, loader = true){
    data['expired'] = 0;
    data['offset'] = this.offset;
    return new Promise( resolve => {

      if(this.offset == -1){
        resolve();
      }else{
        this.network.getUserReceivedPasses(this.user_id, data, loader).then((res: any) => {

          console.log("of", this.offset);
          if(this.offset == 0){

            this.plist = [];
            this.plist = res['passes'];
          }else{
            this.plist = this.plist.concat(res['passes']);
          }


          this.offset = res['offset'];
          resolve();

        }, err => {})
      }


    })

  }

  getPassDates(){
    this.network.getUserReceivedPassesDates(this.user_id, {expired: 0}).then((res: any) => {
      this.datelist = res['dates'];
      this.dcount = 65 * this.datelist.length;
    })
  }

  filterByAlpha(a,b)
  {

    this.alphalist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });

    this.offset = 0;
    if(a.enable == false){
      this.storedData = {}
    }else{
      let param = b == 'sname' ? 'sender_last_name' : b == 'scommunity' ? 'community' : 'event_name';
      this.storedData = {filter_type: param, filter_search: a.alpha.toLowerCase() }

    }

    this.getPasses(this.storedData);

  }

  filterByDate(a, b)
  {
    this.datelist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });

    this.offset = 0;
    if(a.enable == false){
      this.storedData = {};
    }else{
      this.storedData = {filter_type: 'date', filter_search: a.pass_start_date };
    }

    this.getPasses(this.storedData);
  }

  closePassTabs(){
    this.events.publish('pass:allTabClose', Date.now());
  }

  async getDirection(address){

    console.log(address)
    var addr = address.trim();
    const data = await this.modals.present(GmapPageComponent, {myAddress: addr, newAddress: true, isDirections: true});

  }

  passDetails(sel){
    // CreateGroupPage as modal
    console.log(sel);
    const data = this.modals.present(MypassesdetailPageComponent, {item: sel.id, flag: "active"});

  }

  setFilteredItems(flag){


    this.offset = 0;
    this.storedData = { search: this.searchTerm.toLowerCase() };
    this.getPasses(this.storedData);

  }

  loadMore($event){

    this.getPasses(this.storedData, false).then( v => {
      $event.target.complete();
    });


  }

}
