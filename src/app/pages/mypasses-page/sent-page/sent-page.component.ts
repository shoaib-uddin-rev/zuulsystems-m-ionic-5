import { PassGuestLogsPageComponent } from './../pass-guest-logs-page/pass-guest-logs-page.component';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { SentContactsPageComponent } from '../sent-contacts-page/sent-contacts-page.component';
import { GmapPageComponent } from '../../gmap-page/gmap-page.component';
import { MypassesdetailPageComponent } from '../../mypassesdetail-page/mypassesdetail-page.component';
import { NewPassPageComponent } from '../../new-pass-page/new-pass-page.component';

@Component({
  selector: 'app-sent-page',
  templateUrl: './sent-page.component.html',
  styleUrls: ['./sent-page.component.scss'],
})
export class SentPageComponent extends BasePage implements OnInit {

  plist: any[] = [];
  storedData;
  offset = 0;
  canBeResident: boolean = false;
  isHeadOfFamily: number = 0;
  user: any;
  title: string = "My";
  alphalist: any[];
  searchTerm: string = '';
  seeingOthersPass: boolean = false;
  isOtherIsHeadOfFamily: number = 0;
  datelist: any[];
  dcount = 60 * 3;
  allowParentalControl = 0;
  user_id;
  display_name

  constructor(injector: Injector) { 
    super(injector);

    this.alphalist = this.utility.alphalist;
    var display_name = this.nav.getQueryParams().display_name;
    this.isOtherIsHeadOfFamily = this.nav.getQueryParams().isOtherIsHeadOfFamily;

    if(display_name){
      this.title = display_name + "'s";
    }

    this.initialize();

    this.events.subscribe('passes:refreshsent', this.doRefresh.bind(this) );
    this.events.subscribe('pass.deleted.id', this.deleteLocallyByPassId.bind(this));


  }

  ngOnInit() {}

  async initialize(){
    await this.doRefresh();
    this.getPassDates();
  }

  deleteLocallyByPassId(obj){

    var pass_id = obj['pass_id'];
    if(pass_id){
      var elementPos = this.plist.map(function (x) { return x.id; }).indexOf(pass_id);
      this.plist.splice(elementPos, 1);

    }
  }

  doRefresh(refresher = null){

    return new Promise( async resolve => {

      this.user_id = this.nav.getQueryParams().user_id;
      if(this.user_id){

        this.canBeResident = false;
        this.allowParentalControl = parseInt(this.users._user['allow_parental_control']);

      }
      else{

        this.user = await this.sqlite.getActiveUser();
        this.user_id = this.user.id;
        this.canBeResident = this.user['canBeResident'];
        this.isHeadOfFamily = this.user['head_of_family'];

      }

      this.offset = 0;
      this.plist = [];
      this.storedData = {};

      await this.getPasses(this.storedData);
      resolve();

      if(refresher){
        refresher.complete();
      }
    })



  }

  closePassTabs(){
    this.events.publish('pass:allTabClose', Date.now());
  }

  getPasses(data = {}, loader = true){

    return new Promise( resolve => {
      data['expired'] = 0;
      data['offset'] = this.offset;
      this.network.getUserSentPasses(this.user_id, data,loader).then((res: any) => {
        // console.log(res);
        if(this.offset == 0){
          this.plist = res['passes'];
        }else{
          this.plist = this.plist.concat(res['passes']);
        }

        this.offset = res['offset'];
        resolve();
      });
    })


  }

  getPassDates(){
    this.network.getUserSentPassesDates(this.user_id, {expired: 0}).then((res: any) => {
      console.log("datelist", res);
      this.datelist = res['dates'];
      this.dcount = 65 * this.datelist.length;
    })
  }

  async showCheckbox(obj) {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let _data = {item: item, isHeadOfFamily: this.isHeadOfFamily, allowParentalControl: this.allowParentalControl};
    const data = await this.modals.present(SentContactsPageComponent, _data);

    let d = data.data;
    if( d['data'] != 'A'){
      item = data;
    }

  }

  async showGuestLogs(obj, type = 'pass') {

    let event = obj.event;
    let item = obj.item;

    event.stopPropagation();

    let _data = {pass_id : item.pass_id, type: type};
    const data = await this.modals.present(PassGuestLogsPageComponent, _data);

  }

  getDirection(lat,long){
    // console.log(lat,long);
    this.utility.getCurrentLocation(lat,long)
    // .then(data=>{
    //   // console.log(data);
    // })
    // .catch(err=>// console.log(err));

  }

  async editDirections(item, address){

    const _data = await this.modals.present(GmapPageComponent, {address: address});

    let data = _data.data;
    if(data['data'] != 'A'){
      // console.log(item);
      item['lat'] = data.lat;
      item['lng'] = data.lng;
      item['description'] = data.address;

      this.network.updatePassDirections(item).then( return_data => {
      }, err => {})
    }


  }

  async passDetails(sel){
    // CreateGroupPage as modal
    const _data = await this.modals.present(MypassesdetailPageComponent, {item: sel, flag: "active"});
    
  }

  async deletePass(item) {

    let flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'This will remove your pass and all associated sent passes along with it ...')

    if(flag){

      var data = {pass_id: item['pass_id']}
      this.network.retractSentPass(this.user.id, data).then((res: any) => {
        this.spliceItem(item)
      }, err => {});

    }

  }

  editPass(item) {

    this.network.getEditPasseDetails(item['pass_id']).then( async v => {

      if(v['bool'] == true){
        const data = await this.modals.present(NewPassPageComponent, {item: v['pass'], edit: true})
      }else{
        this.utility.presentFailureToast(v['message']);
      }

    });

  }

  spliceItem(item){
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
        this.plist.splice(index, 1);
    }
  }

  filterByAlpha(a,b)
  {

    this.alphalist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });

    this.offset = 0;
    if(a.enable == false){
      this.storedData = {};
      this.getPasses(this.storedData);
    }else{

      if(b == 'ename'){
        this.storedData = { filter_type: 'event_name', filter_search: a.alpha.toLowerCase()  };
        this.getPasses(this.storedData);
      }

    }

  }

  filterByDate(a,b)
  {

    this.datelist.forEach((item, index) => {
      if(item == a){
        a.enable = !a.enable;
      }else{
        item.enable = false
      }
    });

    this.offset = 0;
    if(a.enable == false){
      this.storedData = {};
      this.getPasses(this.storedData)
    }else{
      this.storedData = {filter_type: 'date', filter_search: a.pass_start_date  };
      this.getPasses(this.storedData);
    }

  }

  setFilteredItems(flag){

    this.offset = 0;
    this.storedData = {search: this.searchTerm.toLowerCase()};
    this.getPasses(this.storedData);

  }

  loadMore($event){

    this.getPasses(this.storedData, false).then( v => {
      $event.complete();
    });


  }

}
