import { Component, OnInit, Injector, Input, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { GmapPageComponent } from '../gmap-page/gmap-page.component';
import { MypassesdetailPageComponent } from '../mypassesdetail-page/mypassesdetail-page.component';

@Component({
  selector: 'app-mypasses-page',
  templateUrl: './mypasses-page.component.html',
  styleUrls: ['./mypasses-page.component.scss'],
})
export class MypassesPageComponent extends BasePage implements OnInit {


  tab1Root = 'ActivePage';
  tab2Root = 'ArchivedPage';
  tab3Root = 'SentPage';

  @ViewChild("passTabs", { static: true }) passTabs: IonTabs;

  canBeResident: boolean = false;
  isHeadOfFamily: number = 0;
  canSendPasses: number = 0;
  allowParentalControl: number = 0;
  isOtherIsHeadOfFamily: number = 0;
  tabsIndex = 0;
  user: any;
  isOther: boolean = false;

  activeRoot = 'ActivePage';
  archivedRoot = 'ArchivedPage';
  sentRoot = 'SentPage';
  user_id = null;
  display_name = null;
  tabParams = { user_id: this.user_id, display_name: this.display_name, isOtherIsHeadOfFamily: this.isOtherIsHeadOfFamily };


  constructor(injector: Injector) {
    super(injector);

    this.user_id = this.nav.getQueryParams().user_id
    var is_other = this.nav.getQueryParams().isOther
    this.isOtherIsHeadOfFamily = this.nav.getQueryParams().isOtherIsHeadOfFamily
    this.isOther = (is_other) ? is_other : false;

    this.display_name = this.nav.getQueryParams().display_name
    // console.log(this.display_name);

    this.tabParams.user_id = this.user_id;
    this.tabParams.display_name = this.display_name;
    this.tabParams.isOtherIsHeadOfFamily = this.isOtherIsHeadOfFamily;

    this.sqlite.getActiveUser().then(u => {
      this.user = u;
      this.canBeResident = this.user['can_user_become_resident'];
      this.isHeadOfFamily = parseInt(this.user['head_of_family']);
      this.canSendPasses = parseInt(this.user['can_send_passes']);
      this.allowParentalControl = parseInt(this.user['allow_parental_control']);
    });

    // console.log(this.isHeadOfFamily, this.isOtherIsHeadOfFamily, this.canSendPasses )
    this.events.subscribe('pass:allTabClose', (time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log("strike here");
      this.nav.pop();
    });

    this.events.subscribe('pass:tosenttab', () => {
      this.passTabs.select(this.tab2Root);
    });

  }

  ngOnInit() { }

  ngAfterViewInit() {
    // Now you can use the tabs reference
    var arc = this.nav.getQueryParams().arc;
    if(arc){
      this.passTabs.select(this.tab1Root);
    }
  }


}
