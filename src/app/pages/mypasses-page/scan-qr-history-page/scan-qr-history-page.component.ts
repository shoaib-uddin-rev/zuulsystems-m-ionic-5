import { Component, OnInit, Injector, Input } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-scan-qr-history-page',
  templateUrl: './scan-qr-history-page.component.html',
  styleUrls: ['./scan-qr-history-page.component.scss'],
})
export class ScanQrHistoryPageComponent extends BasePage implements OnInit {

  @Input() items: any = [];
  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  closeModal(res){
    this.modals.dismiss(res)
  }

}
