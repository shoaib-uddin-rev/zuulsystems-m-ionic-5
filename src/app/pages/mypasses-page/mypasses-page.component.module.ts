import { ActivePageComponent } from './active-page/active-page.component';
import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MypassesPageComponent } from './mypasses-page.component';
import { ArchivedPageComponent } from './archived-page/archived-page.component';
import { SentPageComponent } from './sent-page/sent-page.component';
import { PassItemComponent } from 'src/app/components/pass-item/pass-item.component';
import { EmptyviewComponentModule } from 'src/app/components/emptyview/emptyview.component.module';
import { PassGuestLogsPageComponent } from './pass-guest-logs-page/pass-guest-logs-page.component';
import { ScanQrHistoryPageComponent } from './scan-qr-history-page/scan-qr-history-page.component';

@NgModule({
        declarations: [
                MypassesPageComponent,
                ActivePageComponent,
                ArchivedPageComponent,
                SentPageComponent,
                PassItemComponent,
                PassGuestLogsPageComponent,
                ScanQrHistoryPageComponent
                


        ],
        imports: [
                CommonModule,
                IonicModule,
                FormsModule,
                ReactiveFormsModule,
                EmptyviewComponentModule

        ],
        exports: [
                MypassesPageComponent
        ],
        providers: [
                Location
        ]
})
export class MypassesPageComponentModule { }