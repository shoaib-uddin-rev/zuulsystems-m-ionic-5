import { Component, OnInit, ViewChild, Injector, Input } from '@angular/core';
import { IonItemSliding, IonSearchbar } from '@ionic/angular';
import { BasePage } from '../../base-page/base-page';
import { PickContactPageComponent } from '../pick-contact-page/pick-contact-page.component';

@Component({
  selector: 'app-sent-contacts-page',
  templateUrl: './sent-contacts-page.component.html',
  styleUrls: ['./sent-contacts-page.component.scss'],
})
export class SentContactsPageComponent extends BasePage implements OnInit {

  searchTerm: string = '';
  original: any;
  contacts: any;
  items: any = [];
  noselection: boolean = false;

  @Input() item;
  @Input() isHeadOfFamily: number = 0;
  @Input() canRetractRecipients: number = 0;
  @Input() allowParentalControl: number = 0;

  // new variables added
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value;
  search_on = false;
  event_name;

  constructor(injector: Injector) {
    super(injector);

    var c = this.item;
    console.log("sent item", c);
    this.event_name = c.event_name;
    const hof = this.isHeadOfFamily;
    const crp = this.allowParentalControl;

    if (hof == 1) {
      this.canRetractRecipients = 1;
    } else if (crp == 1) {
      this.canRetractRecipients = 1;
    } else {
      this.canRetractRecipients = 0;
    }

    this.original = c;
    var self = this;

    this.network.getContactToPass(this.original['pass_id']).then(response => {
      // self.utilityProvider.presentSuccessToast(response['message']);
      if (response['pass'].length == 0) {
        return;
      }
      let d: any = response['pass']['get_qrs'];
      self.items = d;
      self.contacts = d;
      self.checkItems()

    }, error => { })

  }

  ngOnInit() { }

  setFocusOnSearch() {
    var self = this;
    this.search_on = true;
    setTimeout(() => {
      self.searchbar.setFocus();
    }, 500);

  }


  filterGlobal($event) {
    let val = this.search_value;
    // this.getContactListByGroupId(val, this.group.id, this.groupcontactsoffset,  false, false);

  }

  resetSearch() {
    // this.getContactListByGroupId(null, this.group.id, 0, true, false);
  }

  doRefresh($event) {
    // this.groupcontactsoffset = 0;
    // this.getContactListByGroupId(this.search_value, this.group.id, this.groupcontactsoffset, false, false).then( v => {
    //   $event.complete();
    // });
  }

  public open(itemSlide: IonItemSliding) {
    // reproduce the slide on the click
    itemSlide.open('end');
  }

  async showConfirm(item) {

    const flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'I want to retract this recipient’s pass and make all associated QR codes null and void');

    if (flag){
      this.network.removePassRecipient(item['id']).then((res: any) => {
        // console.log(res);

        var recepient_count = res['recepient_count'];
        if (recepient_count == 0) {
          this.events.publish('pass.deleted.id', { 'pass_id': res['pass_id'] })
        }

        this.spliceItem(item);
        if (this.items.length == 0) {
          this.events.publish('passes:refreshsent')
          this.modals.dismiss({data: 'A'});
        }

      }, err => {
        this.utility.presentToast(err.error.message);
      });
    }

  }

  spliceItem(item) {

    const index: number = this.items.indexOf(item);
    if (index !== -1) {
      this.items.splice(index, 1);
    }
  }

  checkItems() {
    this.items.forEach((item) => {
      // console.log(item['is_enabled'], parseInt(item['is_enabled']) == 1)
      if (parseInt(item['is_enabled']) == 1) {
        item['selected'] = true;
      }

    });
  }

  setFilteredItems(flag) {

    var self = this;


    this.items = this.contacts.filter((item) => {
      return (item.added_user.name && item.added_user.name.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1)
        || (item.added_user.email && item.added_user.email.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1)
        || (item.added_user.phone_number && item.added_user.phone_number.toLowerCase().indexOf(self.searchTerm.toLowerCase()) > -1);
    });

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SentContactsPage');
  }

  returnSelected() {

    this.items.forEach((item) => {
      item['is_enabled'] = item['selected'] ? "1" : "0"
    });
    this.original['get_qrs'] = this.items;
    this.modals.dismiss(this.original);
  }

  fireChange(item) {
    // console.log(item);
    item.is_enabled = (item.selected) ? 1 : 0;
    this.network.togglePassEnable(item.id, item.is_enabled).then((res: any) => {
      // console.log(res);
    }, err => { })
  }

  async plusHeaderButton() {

    const _data = await this.modals.present(PickContactPageComponent);
    let data = _data.data;

    if (data == null) { return }
    let d: any = data;

    if (d.length > 0) {
      let passid = this.original.id;

      this.network.addContactToPass(passid, data).then(response => {
        this.utility.presentSuccessToast(response['message']);
        this.items = response['pass']['get_qrs'];
        this.checkItems();
      }, err => { })
    }


  }

}
