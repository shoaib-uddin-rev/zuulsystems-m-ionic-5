import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { PopoverController } from '@ionic/angular';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { BasePage } from '../base-page/base-page';
import { ForgetPasswordPageComponent } from '../forget-password-page/forget-password-page.component';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  sw_user: any = null;
  @Input() navwithin = false;

  constructor(injector: Injector, public popoverCtrl: PopoverController) {
    super(injector);
    this.setupForm()
  }

  ngOnInit() { }

  setupForm() {

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      phone_number: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
      password: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])]
    })

  }



  async openLogUsers(event) {

    const _data = { id: null, flag: 'SW' };
    const data = await this.popover.present(event, _data)

    if (data == null) {
      return;
    }
    const sw_user = data["param"];
    this.setUserInForm(sw_user)

  }

  setUserInForm(sw_user) {
    if (sw_user) {
      this.users.switchUserAccount(sw_user).then(data => {
        if (!data) {
          // this.navwithin = true;
          this.aForm.controls['phone_number'].setValue(sw_user['phone_number'])
        }
      })

    }
  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      console.log(_tel);
      // this.aForm.controls["phone_number"].setValue(_tel);
      ev.target.value = _tel;
    }
  }

  forgetpassword() {
    this.modals.present(ForgetPasswordPageComponent);
  }

  login() {
    this.submitAttempt = true;
    var formdata = this.aForm.value;

    formdata['register_with_phonenumber'] = true;
    this.events.publish('user:login', formdata);
  }

  signup() {
    this.nav.setRoot('1/SignupPage', {
      animate: true,
      direction: 'forward'
    });
  }

}
