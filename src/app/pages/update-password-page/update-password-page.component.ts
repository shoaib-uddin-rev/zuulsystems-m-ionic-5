import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-update-password-page',
  templateUrl: './update-password-page.component.html',
  styleUrls: ['./update-password-page.component.scss'],
})
export class UpdatePasswordPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  @Input() user;

  constructor(injector: Injector) { 
    super(injector);

    this.setupForm();
  }

  ngOnInit() {}

  setupForm(){

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(30), Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/)])],
      password_confirm: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(30), Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/)])],
    }, { validator: this.utility.checkIfMatchingPasswords('password', 'password_confirm') })


  }


  save(){


    this.submitAttempt = true;
    var formdata = this.aForm.value;

    var in_ps = !this.aForm.controls.password.valid;
    var in_pps = !this.aForm.controls.password_confirm.valid

    if(in_ps){ this.utility.presentFailureToast( "invalid password"); return }
    if(in_pps){ this.utility.presentFailureToast( "password must match"); return }

    this.network.updatePassword(formdata).then( res => {
      this.utility.presentSuccessToast(res['message']);
      this.modals.dismiss();

    }, err => {})

  }

}
