import { Component, Injector, OnInit, ViewChild, Input, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { CountryCodePageComponent } from '../country-code-page/country-code-page.component';
import { GmapPageComponent } from '../gmap-page/gmap-page.component';
import { WelcomescreenComponent } from './welcomescreen/welcomescreen.component';
const statesjson = require('./../../data/states.json');
const countryjson = require('./../../data/countries.json');



@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent extends BasePage implements OnInit, AfterViewInit {

  public formdata: any;
  @ViewChild('slides', { static: true }) slides: IonSlides;
  @Input() new = true;
  revisit: boolean = false;
  user: any;
  heading1 = "ALL DONE! Click The Button Below To Start Using The App";
  Start = "Start";
  
  

  constructor(injector: Injector, public _DomSanitizer: DomSanitizer) {
    super(injector);
    
    this.events.subscribe("registration:slidetoNext", this.goToNext.bind(this));
    this.events.subscribe("registration:setdata", this.setData.bind(this));
    this.events.subscribe("registration:finish", this.dashboardopen.bind(this) );

  }
  ngAfterViewInit(): void {

    if (this.new == true) {
      var self = this;
      setTimeout(() => {
        self.modals.present(WelcomescreenComponent);
      }, 1000)

    }

    this.fetchProfileValues();

  }

  ngOnInit() { }

  goToNext(num) {

    console.log(num);
    if(num == 4){

      var formdata = this.user;
      formdata["id"] = this.user.id;

      if (!this.revisit) {
        this.revisit = false;
      }
      this.updateProfileAsIs(formdata);

    }else{
      this.slides.lockSwipes(false);
      this.slides.slideTo(num, 500);
      this.slides.lockSwipes(true);  
    }
    
  }

  async fetchProfileValues() {
    // get user data
    this.network.getUser().then(
      (user: any) => {
        this.user = user.success;
        
      } 
    );
  }

  setData(data){
    this.user = { ...this.user, ...data};
  }

  seProfileImageInData(formdata) {
    console.log("here-", formdata.profile_image);
    return new Promise((resolve) => {
      this.utility
        .convertImageUrltoBase64(formdata.profile_image)
        .then((image) => {
          resolve(image);
        });
    });
  }

  seLicenceImageInData(formdata) {
    console.log("here1-", formdata.profile_image);
    return new Promise((resolve) => {
      this.utility
        .convertImageUrltoBase64(formdata.licence_image)
        .then((image) => {
          resolve(image);
        });
    });
  }

  updateProfileAsIs(formdata) {
    var self = this;
    let promise = Promise.all([
      this.seProfileImageInData(formdata),
      this.seLicenceImageInData(formdata),
    ]);
    promise.then((images) => {
      formdata.profile_image = images[0];
      formdata.licence_image = images[1];
      // console.log(formdata);

      if (!this.revisit) {
        this.revisit = false;
      }

      this.network.updateProfile(formdata).then((res) => {
        this.slides.lockSwipes(false);
        this.slides.slideTo(4, 500);
        this.slides.lockSwipes(true);
        },
        (err) => { }
      );
    });
  }

  dashboardopen() {
    this.new = false;
    this.events.publish("user:get");
  }




}
