import { StateService } from './../../../services/state.service';
import { FormGroup, Validators } from '@angular/forms';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-secondscreen',
  templateUrl: './secondscreen.component.html',
  styleUrls: ['./secondscreen.component.scss'],
})
export class SecondscreenComponent extends BasePage implements OnInit {

  aForm: FormGroup;

  profile: any;

  _user: any;
  get user(): any {
    return this._user;
  }
  @Input() set user(value: any) {
    this._user = value;

    console.log(value);
    this.fetchProfileValues();
  };


  states: any = [];
  constructor(injector: Injector, public statesService: StateService) {
    super(injector);
    this.setupForm();
    this.states = statesService.getStates();
  }

  ngOnInit() { }

  setupForm() {
    var re = /\S+@\S+\.\S+/;

    let tempdate = new Date();
    // let myDate: String = new Date(tempdate.getTime() - tempdate.getTimezoneOffset()*60000).toISOString();


    this.aForm = this.formBuilder.group({
      
      apartment: [""],
      street_address: [""],
      city: ["", Validators.compose([Validators.required])],
      state: ["", Validators.compose([Validators.required])],
      zip_code: [
        "",
        Validators.compose([Validators.required, Validators.pattern("[0-9]*")]),
      ],
    });
  }

  async fetchProfileValues() {
    // get user data

    if (!this.user) {
      return;
    }

    // redirect to welcomeid
    this.profile = this.user.profile;
    console.log(this.profile);
    
    this.aForm.controls["apartment"].setValue(this.profile["apartment"]);
    this.aForm.controls["street_address"].setValue(
      this.profile["street_address"]
    );
    this.aForm.controls["city"].setValue(this.profile["city"]);
    this.aForm.controls["state"].setValue(this.profile["state"]);
    this.aForm.controls["zip_code"].setValue(this.profile["zip_code"]);

  }

  onZipCodeKeyUp(event) {
    // console.log(event.target.value);
    var e = event.target.value;
    e = e.replace(/\D/g, "");
    // console.log(e);
    event.target.value = e;
  }

  
  async goToNext(num) {

    const flag = await this.validateForm();
    console.log(flag);
    if (!flag) {
      return;
    }

    this.events.publish("registration:setdata", this.aForm.value);
    this.events.publish("registration:slidetoNext", num);

  }

  validateForm(): Promise<boolean> {

    const eror = "Please Enter Valid";
    return new Promise(resolve => {

      var in_apartment = !this.aForm.controls.apartment.valid;
      var in_address = !this.aForm.controls.street_address.valid;
      var in_city = !this.aForm.controls.city.valid;
      var in_state = !this.aForm.controls.state.valid;
      var in_zip_code = !this.aForm.controls.zip_code.valid;

      if (in_apartment) {
        this.utility.presentFailureToast(eror + " appartment");
        resolve(false);
      }
      // if(in_address){ this.showAlert(eror + " address"); return }
      if (in_city) {
        this.utility.presentFailureToast(eror + " city");
        resolve(false);
      }
      if (in_state) {
        this.utility.presentFailureToast(eror + " state");
        resolve(false);
      }
      if (in_zip_code) {
        this.utility.presentFailureToast(eror + " zip code");
        resolve(false);
      }

      resolve(true);





    });

  }

}
