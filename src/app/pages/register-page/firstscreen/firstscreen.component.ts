import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { BasePage } from '../../base-page/base-page';
import { CountryCodePageComponent } from '../../country-code-page/country-code-page.component';

const countries = require('src/app/data/countries.json')

@Component({
  selector: 'app-firstscreen',
  templateUrl: './firstscreen.component.html',
  styleUrls: ['./firstscreen.component.scss'],
})
export class FirstscreenComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  profile: any;

  _user: any;
  get user(): any {
    return this._user;
  }
  @Input() set user(value: any) {
    this._user = value;

    console.log(value);
    this.fetchProfileValues();
  };

  piImageBase64 = this.users.avatar;

  dial_code = {
    name: 'United States',
    dial_code: '+1',
    code: 'US',
    image: 'assets/imgs/flags/us.png',
  };

  constructor(injector: Injector, public _DomSanitizer: DomSanitizer) {
    super(injector);
    this.setupForm();
  }

  setupForm() {
    var re = /\S+@\S+\.\S+/;

    let tempdate = new Date();
    // let myDate: String = new Date(tempdate.getTime() - tempdate.getTimezoneOffset()*60000).toISOString();


    this.aForm = this.formBuilder.group({
      name: [
        "",
        Validators.compose([
          Validators.pattern("[a-zA-Z ]*"),
          Validators.required,
        ]),
      ],
      middle_name: ["", Validators.compose([Validators.pattern("[a-zA-Z ]*")])],
      last_name: [
        "",
        Validators.compose([
          Validators.pattern("[a-zA-Z ]*"),
          Validators.required,
        ]),
      ],
      date_of_birth: [null, Validators.compose([Validators.required])],
      phone: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.pattern("[0-9]*"),
        ]),
      ],
      email: [
        "",
        Validators.compose([
          Validators.pattern(re),
          Validators.required,
        ]) /*, VemailValidator.checkEmail */,
      ],
      dial_code: ["+1"],
      profile_image: [""],
    });
  }

  async fetchProfileValues() {

    // this.user = user;
    // get user data
    // redirect to welcomeid
    if(!this.user){
      return;
    }
    this.profile = this.user['profile'];
    this.aForm.controls["name"].setValue(this.user["name"]);
    this.aForm.controls["middle_name"].setValue(
      this.profile["middle_name"]
    );
    this.aForm.controls["last_name"].setValue(this.profile["last_name"]);

    var dob = this.profile["date_of_birth"];

    if (dob) {
      let _date = Date.parse(dob.replace(" ", "T"));
      let iso = new Date(_date).toISOString();
      this.aForm.controls["date_of_birth"].setValue(iso);

      // this.aForm.controls["date_of_birth"].setValue(dob);
      // console.log(dob);
      //this.date_of_birth_mdy = this.utility.formatDateMDY(dob);
      // this.date_of_birth_mdy = this.profile["dateOfBirthMDY"];
      // this.selectedDate = new Date(dob);
    }

    var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(
      this.user["phone_number"]
    );
    this.aForm.controls["phone"].setValue(_tel);

    if (!this.user["dial_code"]) {
      this.user["dial_code"] = "+1";
    }
    this.aForm.controls["dial_code"].setValue(this.user["dial_code"]);

    this.aForm.controls["email"].setValue(this.user["email"]);

    this.aForm.controls["profile_image"].setValue(
      this.user["profileImageUrl"]
    );

    // this.aForm.controls["licence_number"].setValue(this.user["licence_number"]);
    this.piImageBase64 = this.user["profileImageUrl"];

    //
    // do code search
    this.setDialCode();

  }

  ngOnInit() { }

  getprofileimage() {
    this.utility.snapImage('profile').then((image) => {
      this.piImageBase64 = image as string;
      this.aForm.controls['profile_image'].setValue(this.piImageBase64);
    });
  }

  async openCounryCode() {
    // CreateGroupPage as modal

    const _data = await this.modals.present(CountryCodePageComponent);
    const data = _data.data;
    if (data.data != 'A') {
      this.dial_code = data;
      this.aForm.controls['dial_code'].setValue(this.dial_code.dial_code);
    }
  }

  setDialCode() {
    const n: any[] = countries;
    this.dial_code = n.find((x) => x.dial_code == this.user['dial_code']);
    this.dial_code["image"] =
      "assets/imgs/flags/" + this.dial_code["code"].toLowerCase() + ".png";
  }

  onTelephoneChange(ev, tel) {

    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      this.aForm.controls['phone_number'].setValue(_tel);
    }

  }

  validateDate() {

    let dob = this.aForm.controls.date_of_birth.value;
    let date = new Date(dob);
    if (!this.utility.isOverThirteen(date)) {
      this.utility.presentFailureToast(
        "Age must be over 13 years old"
      );
      this.aForm.controls.date_of_birth.setValue(null);
    }
  }

  async goToNext(num) {

    const flag = await this.validateForm();
    console.log(flag);
    if (!flag) {
      return;
    }

    this.events.publish("registration:setdata", this.aForm.value);
    this.events.publish("registration:slidetoNext", num);
  }

  validateForm(): Promise<boolean> {

    const eror = "Please Enter Valid";
    return new Promise(resolve => {

      var in_name = !this.aForm.controls.name.valid;
      var in_mname = !this.aForm.controls.middle_name.valid;

      var in_dialcode = !this.aForm.controls.dial_code.valid;
      var in_lname = !this.aForm.controls.last_name.valid;
      var in_birth = !this.aForm.controls.date_of_birth.valid;

      var in_phone = !this.aForm.controls.phone.valid;
      in_phone = !this.utility.isPhoneNumberValid(
        this.aForm.controls.phone.value
      );

      var in_email = !this.aForm.controls.email.valid;

      if (in_name) {
        this.utility.presentFailureToast(
          eror + " Name, can not be null, only alphabets allowed"
        );
        resolve(false);
      }
      if (in_dialcode) {
        this.utility.presentFailureToast(eror + " Dial Code");
        resolve(false);
      }
      if (in_lname) {
        this.utility.presentFailureToast(
          eror + " Last Name, can not be null, only alphabets allowed"
        );
        resolve(false);
      }
      if (in_birth) {
        this.utility.presentFailureToast(eror + " Date Of Birth");
        resolve(false);
      }
      if (in_email) {
        this.utility.presentFailureToast(eror + " Email");
        resolve(false);
      }
      if (in_phone) {
        this.utility.presentFailureToast(
          eror + " Phone Number (Minimum 10 Digits)"
        );
        resolve(false);
      }

      // CHeck if Email already exist with another user
      let emailObj = { email: this.aForm.controls.email.value };
      this.network.checkEmailAlreadyInUse(emailObj).then(
        (res) => {
          const user_count = res["user_count"];
          if (parseInt(user_count) > 0) {
            this.promptUserToUseEmail(this.aForm.controls.email.value).then((res) => {
                resolve(true);
            });
          } else {
            resolve(true)
          }
        },
        (err) => { }
      );

    })




  }

  promptUserToUseEmail(email): Promise<boolean> {
    return new Promise(async (resolve) => {

      const flag = await this.utility.presentConfirm('Get Permission', 'Cancel', 'Email ALert', email +
        ' is already in use, try other email or get permission to use this email address')

      if (flag) {
        this.sendVerifyEmailAddress(email).then((res) => {
          this.user["suspand"] = "1";
          resolve(res);
        });
      }



    });
  }

  sendVerifyEmailAddress(email): Promise<boolean> {
    return new Promise((resolve) => {
      let emailObj = { email: this.aForm.controls.email.value };
      this.network.verifyEmailAddress(emailObj).then(
        (res) => {
          this.utility.presentSuccessToast(res["message"]);
          resolve(true);
        },
        (err) => {
          resolve(false);
        }
      );
    });
  }


}
