import { FormGroup, Validators } from '@angular/forms';
import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-thirdscreen',
  templateUrl: './thirdscreen.component.html',
  styleUrls: ['./thirdscreen.component.scss'],
})
export class ThirdscreenComponent extends BasePage implements OnInit {

  aForm: FormGroup
  is_licence_locked = 0;

  profile: any;

  _user: any;
  get user(): any {
    return this._user;
  }
  @Input() set user(value: any) {
    this._user = value;

    console.log(value);
    this.fetchProfileValues();
  };


  constructor(injector: Injector, public _DomSanitizer: DomSanitizer) {
    super(injector);
    this.setupForm();
  }

  liImageBase64 = "";

  ngOnInit() { }
  
  setupForm() {
    var re = /\S+@\S+\.\S+/;

    let tempdate = new Date();
    // let myDate: String = new Date(tempdate.getTime() - tempdate.getTimezoneOffset()*60000).toISOString();
    this.aForm = this.formBuilder.group({

      licence_format: ["", Validators.compose([Validators.required])],
      licence_image: ["", Validators.compose([Validators.required])],
      
    });
  }

  async fetchProfileValues() {
    // get user data

    if (!this.user) {
      return;
    }

    console.log("ahuva")

    // redirect to welcomeid
    this.profile = this.user.profile;
    // console.log(this.profile);
    
    this.aForm.controls["licence_format"].setValue(
      this.profile["licence_format"]
    );

    var iv = this.profile["licenceImageUrl"];
    if(iv){
      if (iv.indexOf("licence") != -1) {
        this.profile["licenceImageUrl"] = null;
      }
    }

    this.aForm.controls["licence_image"].setValue(
      this.profile["licenceImageUrl"]
    );

    this.liImageBase64 = this.profile["licenceImageUrl"];

  }

  validateForm(): Promise<boolean> {

    const eror = "Please Enter Valid";
    return new Promise(resolve => {

      var in_licence_format = !this.aForm.controls.licence_format.valid;
      var in_licence_image = !this.aForm.controls.licence_image.valid;
      
      if (in_licence_format) {
        this.utility.presentFailureToast(eror + " licence format");
        resolve(false);
      }
      // if(in_address){ this.showAlert(eror + " address"); return }
      if (in_licence_image) {
        this.utility.presentFailureToast(eror + " city");
        resolve(false);
      }
      
      resolve(true);

    });

  }

  getlicenceimage() {
    this.network.getLicenceLockStatus().then((v) => {
      if (v["locked"] == 1) {
        this.promptLicenceUploadLock();
      } else {
        this.utility.snapImage("licence").then((image) => {
          this.liImageBase64 = image as string;
          this.aForm.controls["licence_image"].setValue(this.liImageBase64);
        });
      }
    });
  }

  promptLicenceUploadLock() {
    this.utility.showAlert(
      "Licence details are Locked, please contact admins to unlock the feature."
    );
  }

  async finishregister(num) {
    
    const flag = await this.validateForm();
    console.log(flag);
    if (!flag) {
      return;
    }

    this.events.publish("registration:setdata", this.aForm.value);
    this.events.publish("registration:slidetoNext", num);

  }

}
