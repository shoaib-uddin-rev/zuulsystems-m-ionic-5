import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { RequestBecomeResidentPageComponent } from '../request-become-resident-page/request-become-resident-page.component';

@Component({
  selector: 'app-code-verification-page',
  templateUrl: './code-verification-page.component.html',
  styleUrls: ['./code-verification-page.component.scss'],
})
export class CodeVerificationPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  user: any;
  @Input() emv: boolean = false;
  h2 = "Become A Resident";
  para = "To become a resident, please enter the unique code emailed to you by your community administrator.";


  constructor(injector: Injector) { 
    super(injector)

    this.setupForm();
      this.user = this.users.getCurrentUser();

      if(this.emv == true){
        this.h2 = "Verify Email";
        this.para = "To verify the use of duplicate email, please enter the unique code sent to you by the email owner."
      }

  }

  ngOnInit() {}

  closeView(){
    this.modals.dismiss();
  }

  setupForm(){

    // var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      code: ['', Validators.compose([ Validators.required]) /*, VemailValidator.checkEmail */ ]
    })

  }

  async sendCodeToServer(){

    this.submitAttempt = true;
    var formdata = this.aForm.value;

    var data = {
      verification_code: formdata.code,
      user_id: this.user.id
    }

    if(this.emv == true){
      this.network.sendCodeToVerifyEmail(data).then((res: any) =>{
        this.events.publish('user:get');
      },
      (err) =>{ });
    }else{
      this.network.sendCodeToBecomeResident(data).then((res: any) =>{
        this.events.publish('user:get');
      },
      (err) =>{ });
    }

  }

  redirectToContactUsFOrm(){
    // this.utilityProvider.openContactFormUrl();
    this.modals.present(RequestBecomeResidentPageComponent);
  }


}
