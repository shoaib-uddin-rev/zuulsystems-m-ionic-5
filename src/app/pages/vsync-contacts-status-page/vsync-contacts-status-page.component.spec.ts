import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VSyncContactsStatusPageComponent } from './vsync-contacts-status-page.component';

describe('VSyncContactsStatusPageComponent', () => {
  let component: VSyncContactsStatusPageComponent;
  let fixture: ComponentFixture<VSyncContactsStatusPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VSyncContactsStatusPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VSyncContactsStatusPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
