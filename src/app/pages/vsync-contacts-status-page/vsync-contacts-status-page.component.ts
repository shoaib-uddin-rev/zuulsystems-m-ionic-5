import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-vsync-contacts-status-page',
  templateUrl: './vsync-contacts-status-page.component.html',
  styleUrls: ['./vsync-contacts-status-page.component.scss'],
})
export class VSyncContactsStatusPageComponent extends BasePage implements OnInit {

  @Input() phone_contacts: any[] = [];
  @Input() title = "Sync Contact"

  constructor(injector: Injector) { 
    super(injector)
  }

  ngOnInit() {}

  close(){
    this.modals.dismiss();
  }

}
