import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
})
export class SettingsPageComponent extends BasePage implements OnInit {


  aForm: FormGroup;
  tempaccesstoken: any;
  user: any;

  constructor(injector:Injector) { 
    super(injector);

    this.setupForm();
    this.fetchProfileValues();
  }

  ngOnInit() {}

  setupForm(){

    this.aForm = this.formBuilder.group({
      push_notification: [true, Validators.required],
      email_notification: [true, Validators.required],
      vacation_mode: [false, Validators.required],
    })

  }

  async fetchProfileValues(){

      this.network.getUser(true).then((user: any) => {

        this.user = user.success;
        // console.log(this.user);

        this.aForm.controls['push_notification'].setValue(!!parseInt(this.user["push_notification"]));
        this.aForm.controls['email_notification'].setValue(!!parseInt(this.user["email_notification"]));
        this.aForm.controls['vacation_mode'].setValue(!!parseInt(this.user["vacation_mode"]));



      }, err => { this.nav.pop() })

  }

  updateFlags(){

    var formdata = this.aForm.value;
    console.log(formdata);

    formdata['push_notification'] = formdata['push_notification'] ? "true" : "false";
    formdata['email_notification'] = formdata['email_notification'] ? "true" : "false";
    formdata['vacation_mode'] = formdata['vacation_mode'] ? "true" : "false";

    this.network.updateUserNotificationSettings(formdata).then((res) =>{
      this.utility.presentSuccessToast(res['message'])
      this.nav.pop();
    }, err => {})

  }

}
