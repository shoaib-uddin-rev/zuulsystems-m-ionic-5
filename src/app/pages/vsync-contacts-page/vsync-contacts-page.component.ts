import { Component, OnInit, Injector, Input, ViewChild, AfterViewInit } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-vsync-contacts-page',
  templateUrl: './vsync-contacts-page.component.html',
  styleUrls: ['./vsync-contacts-page.component.scss'],
})
export class VSyncContactsPageComponent extends BasePage implements OnInit, AfterViewInit {

  @ViewChild('searchbar') searchbar:IonSearchbar;
  search_value;
  search_on = false;
  @Input() fromContacts: boolean = false;
  @Input() singleSelection: boolean = false;
  @Input() isFromRequestPassScreen: boolean = false;
  @Input() selected_contacts = [];
  filter_phone_contacts = [];

  public contactsoffset = 0;
  public phone_contacts: any[] = [];
  public contact_ids = [];

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector);

    this.search_on = false;
    
  }

  ngAfterViewInit() {
    this.initialize();
  }

  ngOnInit() {}

  async initialize(){
    if(this.fromContacts){
      this.contactsoffset = 0;
      this.getNetworkContacts(null, false, false);
    }else{

      // check if contacts already sync, if not do it now
      let count = await this.storedcontacts.checkIfPhoneSyncAlready()
      if(count == 0){
        this.utility.showLoader();
          let contacts = await this.utility.getPhoneContacts();
          console.log(contacts);
          await this.sqlite.setSyncContactListInDatabase(contacts, true);
          this.utility.hideLoader();
      }
      this.storedcontacts.getSyncNetworkContacts(null, false, false)
    }
  }

  async getNetworkContacts(search = null, is_concat = true, loader = false) {

    return new Promise(resolve => {
      let offset = this.contactsoffset;
      // if (is_concat == false) { offset = 0 }
      this.sqlite.getContacts(search, offset, 0, loader, this.contact_ids).then((res: any) => {

          if (is_concat) {
            this.contactsoffset = res['offset'] as number;
            this.phone_contacts = this.phone_contacts.concat(res['contact_list']);
          } else {
            this.contactsoffset = res['offset'] as number;
            this.phone_contacts = res['contact_list'];
          }

          resolve(this.phone_contacts);

      }, error => {
          resolve(this.phone_contacts);
      });
    });

  }

  isSelectedContact(id){
    return this.selected_contacts.includes(id);
  }

  addSelectedContact(check, id){

    // console.log(check, id);
    if(check == true){

      if(this.singleSelection){
        this.selected_contacts = [];
        this.selected_contacts.push(id);
      }else{
        if(!this.selected_contacts.includes(id)){
          this.selected_contacts.push(id);
        }
      }




    }else{

        var index = this.selected_contacts.indexOf(id);

        if (index > -1) {
          this.selected_contacts.splice(index, 1);
        }

    }
    // console.log(this.selected_contacts);

  }

  addAllSelectedContact(check){

    // console.log(check);
    if(check == true){


      if(this.search_on == true) {

        this.filter_phone_contacts.forEach( c => {
          if(!this.selected_contacts.includes(c.id)){
            this.selected_contacts.push(c.id);
          }
        })

      }else{

        // console.log(this.phone_contacts);
        if(!this.fromContacts){
          this.storedcontacts.sync_contacts.forEach( c => {
            if(!this.selected_contacts.includes(c.id)){
              this.selected_contacts.push(c.id);
            }
          })
        }else{
          this.phone_contacts.forEach( c => {
            if(!this.selected_contacts.includes(c.id)){
              this.selected_contacts.push(c.id);
            }
          })
        }


      }


    }else{

      if(this.search_on == true) {

        this.filter_phone_contacts.forEach( c => {
          var index = this.selected_contacts.indexOf(c.id);

          if (index > -1) {
            this.selected_contacts.splice(index, 1);
          }
        })

      }else{

        if(!this.fromContacts){
          this.storedcontacts.sync_contacts.forEach( c => {
            var index = this.selected_contacts.indexOf(c.id);

            if (index > -1) {
              this.selected_contacts.splice(index, 1);
            }
          })
        }else{
          this.phone_contacts.forEach( c => {
            var index = this.selected_contacts.indexOf(c.id);

            if (index > -1) {
              this.selected_contacts.splice(index, 1);
            }
          })
        }


      }



    }
    // console.log(this.selected_contacts);

  }

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    this.filter_phone_contacts = this.phone_contacts;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event){
    let val = this.search_value;
    // console.log(val);
    if(this.fromContacts){
      this.getNetworkContacts(val, false, false).then( v => {
        // console.log(v);
      });
    }else{
      this.storedcontacts.getSyncNetworkContacts(val, false, false).then( v => {
        // console.log(v);
      });
    }


  }

  ionViewDidLoad() {


  }

  doRefresh($event){
    if(this.fromContacts){
      this.getNetworkContacts(null, false, false).then( v => {
        $event.target.complete();
      });
    }else{
      this.storedcontacts.getSyncNetworkContacts(null, false, false).then( v => {
        $event.target.complete();
      });
    }

  }

  loadMoreContacts($event){
    if(this.fromContacts){
      this.getNetworkContacts(this.search_value).then( v => {
        $event.target.complete();
      });
    }else{

      this.storedcontacts.getSyncNetworkContacts(this.search_value, true, false).then( v => {
        $event.target.complete();
      });
    }

  }

  goBack(){
    this.modals.dismiss();
  }

  async finishSync(){

    if(this.selected_contacts.length == 0){
      return;
    }

    if(this.fromContacts == true){

      // console.log(this.isFromRequestPassScreen);
      if(this.isFromRequestPassScreen == true){

        let contact = this.phone_contacts.find( x => x.id == this.selected_contacts[0]);
        // // console.log(contact);
        // let flag = await this.storedcontacts.isPnCanSendPass(contact.phone_number.replace( /\D+/g, ''));

        // if(flag){
          this.modals.dismiss({list: this.selected_contacts});
        // }



      }else{
        this.modals.dismiss({list: this.selected_contacts});
      }


    }else{

      let filterd = this.storedcontacts.sync_contacts.filter( x => this.selected_contacts.includes(x.id));
      this.storedcontacts.syncContactsViaNetworkApi({data: filterd}).then( v => {
        this.modals.dismiss({list: v});
      })


    }




  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  returnArrayOfPhoneNumbers(item){
    let pns = item['phone_numbers'].split(',');
    return pns;
  }

  selectFirstOne(item){

    if(item.checked && !item.pn){
      let pns = item['phone_numbers'].split(',');
      item.pn = pns[0];
    }

  }

  returnTypeOfPn(types, i){
    if(types && types != ""){
      let t = types.split(',');
      if(t && t[i]){
        return t[i];
      }else{
        return ""
      }

    }else{
      return "";
    }

  }




}
