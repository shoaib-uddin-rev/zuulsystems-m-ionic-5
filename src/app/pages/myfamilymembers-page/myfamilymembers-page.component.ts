import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { CreateFamilyPageComponent } from '../create-family-page/create-family-page.component';

@Component({
  selector: 'app-myfamilymembers-page',
  templateUrl: './myfamilymembers-page.component.html',
  styleUrls: ['./myfamilymembers-page.component.scss'],
})
export class MyfamilymembersPageComponent extends BasePage implements OnInit {

  avatar = this.users.avatar;
  user: any;
  buffer_contacts;
  plist: any[] = [];
  showsearch = false;

  noselection: boolean = true;
  selectedItem;
  has_family: boolean = false;

  constructor(injector: Injector) {
    super(injector);
    this.initialize()
  }

  ngOnInit() { }

  async initialize() {
    this.user = await this.sqlite.getActiveUser();
    this.getMyFamily();

    this.events.subscribe('permissions:update', this.permissionsUpdate.bind(this))
  }

  permissionsUpdate(data) {

    let item = data['item'];
    console.log(this.plist, data);
    let findIndex = this.plist.findIndex(x => x.id == item.id);

    if (data['flag'] == 'can_manage_family') {
      this.plist[findIndex]['can_manage_family'] = data['value'] == 1 || data['value'] == true ? 1 : 0;
      this.network.setManageFamilyPermission({
        id: item['user_id'],
        can_manage_family: this.plist[findIndex]['can_manage_family']
      }).then()
    }

    if (data['flag'] == 'can_send_passes') {
      this.plist[findIndex]['can_send_passes'] = data['value'] == 1 || data['value'] == true ? 1 : 0;
      this.network.setSendPassesPermission({
        id: item['user_id'],
        can_send_passes: this.plist[findIndex]['can_send_passes']
      }).then()

    }

    if (data['flag'] == 'allow_parental_control') {
      this.plist[findIndex]['allow_parental_control'] = data['value'] == 1 || data['value'] == true ? 1 : 0;

      this.network.setAllowParentalPermission({
        id: item['user_id'],
        allow_parental_control: this.plist[findIndex]['allow_parental_control']
      }).then()

    }







  }

  closeModal(res) {
    // this.modals.dismiss(res);
    this.nav.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MyfamilymembersPage');
  }

  async addfamily() {

    // CreateFamilyPage as modal
    const _data = await this.modals.present(CreateFamilyPageComponent, { show_relation: true });
    let data = _data.data;

    if (data['data'] != 'A') {
      this.getMyFamily();
    }

  }

  getMyFamily() {

    this.plist = [];
    this.network.getMyFamilyMembers().then((response: any) => {
      // console.log(response);
      this.plist = response['members'];
      this.selectedItem = null;
      this.noselection = true;
      this.plist.forEach((eachObj) => {
        eachObj.dark = false;
      });
      this.buffer_contacts = this.plist;



    },
      err => { this.closeModal({ data: 'A' }); });

  }

  getItems(ev: any, flag) {
    // Reset items back to all of the items
    this.plist = this.buffer_contacts

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.plist = this.plist.filter((item) => {
        return (item.display_name.toLowerCase().indexOf(val.toLowerCase()) > -1)
          || (item.formattedPhone.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  selectedRecord(item) {
    // console.log(item);
    this.plist.forEach((eachObj) => {
      if (eachObj == item) {
        item.dark = !item.dark
        this.noselection = !item.dark
      } else {
        eachObj.dark = false;
      }
    });
    this.selectedItem = (item.dark) ? item : null;

  }

  async editFamily(item) {

    const _data = await this.modals.present(CreateFamilyPageComponent, { contact_id: item.id, show_relation: true });
    let data = _data.data;

    if (data['data'] != 'A') {
      this.getMyFamily();
    }


  }

  async presentEditFamilyPopover($event, item) {
    var self = this;
    // console.log(item);

    const _data = await this.popover.present($event, { pid: item, flag: 'CF' }, 'interface-style')
    const data = _data.data;
    // let tr = data;
    if (data == null) { return }
    item = data['pid'];
    // console.log(item);

    self.selectedItem = item;

    switch (data['param']) {
      case 'P':
        self.viewFamilyPasses(item);
        break;
      case 'C':
        this.callMe(item.phone_number);
        break;
      case 'E':
        self.editFamily(item);
        break;
      case 'D':
        self.removeFamilyMember()
        break;
      case 'PC':
        self.showParentalControls(item)
        break;
    }

  }

  callMe(num) {
    this.utility.dialMyPhone(num);
  }

  async viewFamilyPasses(item) {
    // console.log(this.selectedItem);
    // CreateGroupPage as modal
    var isOtherIsHeadOfFamily = this.user["head_of_family"];
    // console.log(this.selectedItem);

    const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      s = s.toLowerCase()
      return s.charAt(0).toUpperCase() + s.slice(1)
    }

    var first_name = capitalize(this.selectedItem.name);
    // console.log(this.selectedItem);
    let pdata = { user_id: this.selectedItem.user_id, display_name: first_name, isOther: true, isOtherIsHeadOfFamily: isOtherIsHeadOfFamily }
    const _data = await this.nav.push('MypassesPageComponent', pdata);
  }

  selectedCanManageFamily() {
    // console.log(this.selectedItem);
    if (parseInt(this.selectedItem.can_manage_family) == 1) {
      this.selectedItem.can_manage_family = 0;
    } else {
      this.selectedItem.can_manage_family = 1
    }

    this.network.setManageFamilyPermission(this.selectedItem).then((response: any) => {

    }, err => { });
  }

  selectedCanSendPasses() {
    // console.log(this.selectedItem);
    if (parseInt(this.selectedItem.can_send_passes) == 1) {
      this.selectedItem.can_send_passes = 0
    } else {
      this.selectedItem.can_send_passes = 1
    }

    this.network.setSendPassesPermission(this.selectedItem).then((response: any) => {
      // console.log(response);
    }, err => { });

  }

  removeFamilyMember() {

    if (!this.selectedItem) { return }

    var contact = this.selectedItem;

    const flag = this.utility.presentConfirm('Agree', 'Cancel', 'Confirm Delete?', 'This person will now be deleted as a household member. They will remain in your ZUUL contacts list.');

    if(flag){
      const data = {
        cs: [
          {
            id: contact['id']
          }
        ]
      }
      this.network.deleteFamilyMemberArray(data).then(res => {
        this.utility.presentSuccessToast(res["message"])
        this.getMyFamily();
      }, err => { })
    }

  }

  showParentalControls(item) {
    // // console.log(item)
    this.nav.push('ParentalControlsPage', { user_id: item.user_id })
  }

}
