import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequestBecomeResidentPageComponent } from './request-become-resident-page.component';

describe('RequestBecomeResidentPageComponent', () => {
  let component: RequestBecomeResidentPageComponent;
  let fixture: ComponentFixture<RequestBecomeResidentPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestBecomeResidentPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestBecomeResidentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
