import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-request-become-resident-page',
  templateUrl: './request-become-resident-page.component.html',
  styleUrls: ['./request-become-resident-page.component.scss'],
})
export class RequestBecomeResidentPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  user: any;
  emv: boolean = false;
  communities = [];
  hint;

  constructor(injector: Injector) { 
    super(injector);

    this.setupForm();
    this.user = this.users.getCurrentUser();
    this.getAllCommunities();

  }

  ngOnInit() {}

  closeView(){
    this.modals.dismiss();
  }

  setupForm(){
    // var re = /\S+@\S+\.\S+/;
    this.aForm = this.formBuilder.group({
      description: ['', Validators.compose([ Validators.required]) /*, VemailValidator.checkEmail */ ],
      community_name: ['', Validators.compose([ Validators.required])]
    })

  }

  getAllCommunities(){
    // get the list of all communities
    this.network.getListOfCommunities().then(v => {
      this.communities = v['communities'];
    })

  }

  async sendCodeToServer(){

    this.submitAttempt = true;
    var formdata = this.aForm.value;

    if(this.aForm.invalid){
      this.utility.presentFailureToast("Please fill all fields");
    }

    this.network.sendRequestToBecomeResident(formdata).then((res: any) =>{
      this.utility.presentSuccessToast("Email sent successfully");
      this.modals.dismiss();
    },
    (err) =>{ });

  }

  searchCommunityHint($event){
    let v = $event.target.value;
    if(v){

      if(v.length > 5){

        console.log(v);
        let val = this.communities.find( x => {
          // console.log(x.name)
          return x.name.toLowerCase().includes(v.toLowerCase())
        });
        console.log(val)
        if(val){
          this.hint = val.name;
        }

      }else{
        this.hint = '';
      }


    }else{
      this.hint = '';
    }
  }

}
