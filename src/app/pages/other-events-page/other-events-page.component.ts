import { BasePage } from 'src/app/pages/base-page/base-page';
import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { CreateEventPageComponent } from '../create-event-page/create-event-page.component';

@Component({
  selector: 'app-other-events-page',
  templateUrl: './other-events-page.component.html',
  styleUrls: ['./other-events-page.component.scss'],
})
export class OtherEventsPageComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', {static: true}) searchbar: IonSearchbar;
  search_value;
  search_on = false;
  offset = 0;
  events: any = [];
  selected_event: any = null;
  public user_id;

  constructor(injector: Injector) {

    super(injector);
    

  }

  ngOnInit() {
    this.getUserid().then( v => {
      this.getUserEvents(null, this.offset, false);
    })
  }

  async getUserid(){
    return new Promise( async resolve => {
      let userid = await this.sqlite.getActiveUserId();
      this.user_id = userid;
      resolve();
    })
  }

  setFocusOnSearch(){

    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event){
    this.getUserEvents(this.search_value, 0, false);
  }

  async getUserEvents(search, offset, is_concat = true){

    return new Promise( resolve => {

      this.sqlite.getUserEvents(search, this.user_id, offset, false).then((res: any) =>{

        if (is_concat) {
          this.offset = res['offset'] as number;
          this.events = this.events.concat(res['events']);
        } else {
          this.offset = res['offset'] as number;
          this.events = res['events'];
        }

        resolve(this.events);

      });
    })


  }

  resetSearch(){
    this.getUserEvents(null, 0, false);
  }

  doRefresh($event){

    this.network.getUserEvents().then( v => {
      this.sqlite.setEventListInDatabase(v['events']).then( q => {
        this.getUserEvents(null, 0, false).then( v => {
          $event.complete();
        });
      })
    })


  }

  loadMoreContacts($event){
    this.getUserEvents(this.search_value, this.offset, true).then( v => {
      $event.complete();
    });
  }

  presentPopover($event, item) {

    $event.stopPropagation();

    const data = this.popover.present($event, {
      id: item,
      flag: "E"
    });
    
    if (data == null) {
      return;
    }
    var item = data["pid"];
    switch (data["param"]) {
      case "S":
        this.setPassEvent(item)
        break;
      case "E":
        this.plusHeaderButton($event, item);
        break;
      case "D":
        this.deleteEventFromList(item)
        break;
    }
  }

  async setPassEvent(item){
    // console.log(this.user_id);
    await this.sqlite.setActiveEventInEvents(item.id);
    this.modals.dismiss(item);
  }

  plusHeaderButton($event, item) {
    // CreateGroupPage as modal
    $event.stopPropagation();
    let _data = { event: item };
    const data = this.modals.present(CreateEventPageComponent, _data);

      // console.log(data);
    if(data["data"] != "A"){

      var event = data["data"];
      console.log(event);
      var elementPos = this.events.map(function (x) { return x.id; }).indexOf(event.id);
      // console.log(elementPos);
      if(elementPos == -1){
        // event.new = true;
        // this.events.unshift(event);
        this.getUserEvents(null, 0, false).then( v => {
          $event.complete();
        });

      }else{
        this.events[elementPos] = event;
      }
    }
      
  }

  async deleteEventFromList(item){
    let flag = await this.utility.presentConfirm('Agree', 'Disagree', 'Are You Sure?', 'This will hide this Event from your list but passes will not be effected' );
    if(flag){
        this.network.removeEventFromList(item['id']).then((res: any) => {

          this.sqlite.removeEventFromDatabase(item['id']);
          var elementPos = this.events.map(function (x) { return x.id; }).indexOf(item.id);
          this.events.splice(elementPos, 1);

        }, err => {});
    }
  }

  closeModal(res){
    this.modals.dismiss(res);
  }




}
