import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-forget-password-page',
  templateUrl: './forget-password-page.component.html',
  styleUrls: ['./forget-password-page.component.scss'],
})
export class ForgetPasswordPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {}

  setupForm(){

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.pattern(re), Validators.required]) /*, VemailValidator.checkEmail */ ]
    })

  }

  save() {

    var in_email = !this.aForm.controls.email.valid
    if(in_email){
      this.utility.presentFailureToast("Email address is invalid, please enter a valid email address");
      return
    }
    this.submitAttempt = true;
    var formdata = this.aForm.value;

    this.network.forgetPassword(formdata).then( res => {

      if(res["bool"] == true){
        this.utility.presentSuccessToast(res['message']);
        this.modals.dismiss();
      }else{
        this.utility.presentFailureToast(res['message']);
      }

    }, err => { })



  }

  login() {
    this.modals.dismiss();
  }

  redirectToContactUsFOrm(){
    this.utility.openContactFormUrl();
  }

}
