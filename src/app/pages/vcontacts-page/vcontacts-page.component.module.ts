import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { VContactsPageComponent } from './vcontacts-page.component';
import { ContactpopoverPageComponent } from 'src/app/components/contactpopover-page/contactpopover-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VGroupContactListComponent } from './vgroup-contact-list/vgroup-contact-list.component';
import { VContactListPageComponent } from './vcontact-list-page/vcontact-list-page.component';
import { VFavoriteListPageComponent } from './vfavorite-list-page/vfavorite-list-page.component';
import { VGroupListPageComponent } from './vgroup-list-page/vgroup-list-page.component';

@NgModule({
	declarations: [
                VContactsPageComponent,
                VContactListPageComponent,
                ContactpopoverPageComponent,
                VGroupListPageComponent,
                VFavoriteListPageComponent,
                VGroupContactListComponent

	],
	imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        ReactiveFormsModule
        
	],
	exports: [
		VContactsPageComponent
    ],
    providers: [
        Location
    ]
})
export class VContactsPageComponentModule {}