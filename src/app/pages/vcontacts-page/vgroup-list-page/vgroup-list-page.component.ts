import { Component, Injector, OnInit } from '@angular/core';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';


@Component({
  selector: 'app-vgroup-list-page',
  templateUrl: './vgroup-list-page.component.html',
  styleUrls: ['./vgroup-list-page.component.scss'],
})
export class VGroupListPageComponent extends BasePage implements OnInit {

  constructor(injector: Injector, public storedcontacts: StoredContactsService) { 
    super(injector);
    this.storedcontacts.getMyGroups(true);
  }

  ngOnInit() {}

  showGroupCOntacts(item){
    this.nav.push('VGroupContactListPage', {group_id: item['id'], group_name: item['group_name']});
  }

  goBack(){
    this.events.publish('contacts:popallpages', Date.now() );
  }

  plusHeaderButton(param) {
    this.storedcontacts.createContactGroup();
  }

}
