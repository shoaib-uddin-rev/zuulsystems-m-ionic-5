import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VFavoriteListPageComponent } from './vfavorite-list-page.component';

describe('VFavoriteListPageComponent', () => {
  let component: VFavoriteListPageComponent;
  let fixture: ComponentFixture<VFavoriteListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VFavoriteListPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VFavoriteListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
