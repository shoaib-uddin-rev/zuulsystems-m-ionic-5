import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';


@Component({
  selector: 'app-vfavorite-list-page',
  templateUrl: './vfavorite-list-page.component.html',
  styleUrls: ['./vfavorite-list-page.component.scss'],
})
export class VFavoriteListPageComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', {static: true}) searchbar:IonSearchbar;
  search_value = null;
  search_on = false;

  constructor(injector: Injector, public storedcontacts: StoredContactsService) { 
    super(injector)

    this.search_on = false;
    this.storedcontacts.getMyContacts(null, true, 1, false);
  }

  ngOnInit() {}

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event){
    let val = this.search_value;
    this.storedcontacts.getMyContacts(val, true, 1, false).then( v => {
      // console.log(v);
    });

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad VFavoriteListPage');
    
  }

  resetSearch(){
    this.search_value = null;
    this.storedcontacts.getMyContacts(null, true, 1, false);
  }

  doRefresh($event){
    this.storedcontacts.getMyContacts(this.search_value, true, 1, false).then( v => {
      $event.target.complete();
    });
  }

  loadMoreContacts($event){

    this.storedcontacts.getNetworkContacts(this.search_value, 1, true).then( v => {
      $event.target.complete();
    });

  }

  goBack(){
    this.events.publish('contacts:popallpages', Date.now() );
  }

  async presentPopover($event, item) {
    // var r = item;
    var self = this;
    const _data = await this.popover.present($event, {
      id: item,
      flag: "F"
    }, 'interface-style');

    let data = _data.data;

    // let tr = data;
    if (data == null) {
      return;
    }
    item = data["pid"];
    // console.log(item);

    switch (data["param"]) {
      case "R":
        this.storedcontacts.removeFavoritesFromContacts(item);
        break;
      case "G":
        item['contact_id'] = item['id']
        this.storedcontacts.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        this.storedcontacts.editFamily(item,1);
        break;
      case "C":
        this.utility.dialMyPhone(item.phone_number);
        break;
      case "SP":
        this.storedcontacts.fetchEventsToSendPassTo(item)
        break;
      case "RP":
        this.storedcontacts.sendPassRequestToContact(item);
        break;
    }

  }

  plusHeaderButton(param) {
    this.storedcontacts.syncContactsToFavourities();
  }

}
