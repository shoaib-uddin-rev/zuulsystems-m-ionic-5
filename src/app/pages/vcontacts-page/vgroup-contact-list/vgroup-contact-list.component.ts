import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-vgroup-contact-list',
  templateUrl: './vgroup-contact-list.component.html',
  styleUrls: ['./vgroup-contact-list.component.scss'],
})
export class VGroupContactListComponent extends BasePage implements OnInit {

  group: any;
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  search_value;
  search_on = false;

  public groupcontactsoffset = 0;
  public group_contact_list: any[] = [];

  constructor(injector: Injector, public storedcontacts: StoredContactsService) { 
    super(injector)
    let id = this.nav.getQueryParams().group_id;
    let group_name = this.nav.getQueryParams().group_name;
    this.getContactListByGroupId(null, id, 0, true, false);
    this.group = {
      id,
      group_name
    }
  }

  ngOnInit() {}

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }


  filterGlobal($event){
    let val = this.search_value;
    this.getContactListByGroupId(val, this.group.id, this.groupcontactsoffset,  false, false);

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad VGroupContactListPage');
    
  }

  resetSearch(){
    this.getContactListByGroupId(null, this.group.id, 0, true, false);
  }

  async deleteContactFromGroup(item){
    let flag = await this.removeContactFromGroup();
    if(flag){
      await this.sqlite.removeContactFromGroup(item.collection_id);
      var elementPos = this.group_contact_list.map(function (x) { return x.collection_id; }).indexOf(item.collection_id);
      this.group_contact_list.splice(elementPos, 1);
      this.network.removeFromGroup([{id: item.collection_id}]).then()
    }

  }

  async removeContactFromGroup() {
    return this.utility.presentConfirm("Agree","Disagree", "Confirm Delete", "Do you want to delete this contact from group?");
  }

  async getContactListByGroupId(search = null, id, offset, loader = true, is_concat = true){
    // console.log("P", id);
    // console.log(this.groupcontactsoffset);
    return new Promise( resolve => {
      this.sqlite.getGroupContactList(search, id, offset, loader ).then((res: any) => {

        if(is_concat){
          this.groupcontactsoffset = res['offset'] as number;
          this.group_contact_list = this.group_contact_list.concat(res['contact_list']);
        }else{
          this.groupcontactsoffset = res['offset'] as number;
          this.group_contact_list = res['contact_list'];
        }

        resolve(this.group_contact_list);
      }, error => {
        resolve(this.group_contact_list);
      });
    });
  }

  doRefresh($event){
    this.groupcontactsoffset = 0;
    this.getContactListByGroupId(this.search_value, this.group.id, this.groupcontactsoffset, false, false).then( v => {
      $event.complete();
    });
  }

  loadMoreContacts($event){

    // console.log(this.groupcontactsoffset);
    if(this.groupcontactsoffset == -1){
      $event.complete();
    }else{
      this.getContactListByGroupId(this.search_value, this.group.id, this.groupcontactsoffset, false).then( v => {
        $event.complete();
      });
    }

  }

  async presentGroupPopover($event, item) {
    // var r = item;
    var self = this;

    const _data = await this.popover.present($event,{
      id: item,
      flag: "G",
      count: this.group_contact_list.length
    })

    const data = _data.data;
    // let tr = data;
    if (data == null) {
      return;
    }
    item = data["pid"];
    // console.log(item);

    switch (data["param"]) {

      case "G":

        this.storedcontacts.addContactsToSelectedGroupByItem(item, false).then( (_items: any[]) => {
          _items.forEach(async element => {
            element["new"] = true;
            this.group_contact_list.unshift(element)
          });
        });
        break;
      case "SP":
        if(!this.group_contact_list || this.group_contact_list.length == 0){
          this.utility.showAlert("At least one member should be in group");
        }else{
          this.storedcontacts.fetchEventsToSendPassToGroup(this.group);
        }

        break;
      case "E":
        this.storedcontacts.editContactGroup(this.group).then( v => {
          if(v){
            if(v['group']){
              this.group = v['group'];
            }

          }
        });
        break;
      case "D":
        var self = this;
        this.utility.presentConfirm('Sure', 'Cancel', 'Delete ' + this.group.group_name + '?').then( flag => {
          if(flag){
            self.storedcontacts.deleteContactGroup(this.group).then( v => {
              self.nav.pop();     
            })
          }
        })

        break;
    }

  }


}
