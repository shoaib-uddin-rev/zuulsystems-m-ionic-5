import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VGroupContactListComponent } from './vgroup-contact-list.component';

describe('VGroupContactListComponent', () => {
  let component: VGroupContactListComponent;
  let fixture: ComponentFixture<VGroupContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VGroupContactListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VGroupContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
