import { Component, OnInit, Injectable, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-vcontacts-page',
  templateUrl: './vcontacts-page.component.html',
  styleUrls: ['./vcontacts-page.component.scss'],
})
export class VContactsPageComponent extends BasePage implements OnInit {

  tab1Root = 'VContactListPage';
  tab2Root = 'VGroupListPage';
  tab3Root = 'VFavoriteListPage';

  constructor(injector: Injector) { 
    super(injector)

    this.events.subscribe('contacts:popallpages', (time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.nav.pop();
    });
  }

  ngOnInit() {}

}
