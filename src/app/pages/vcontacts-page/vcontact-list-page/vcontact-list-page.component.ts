import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, IonVirtualScroll } from '@ionic/angular';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-vcontact-list-page',
  templateUrl: './vcontact-list-page.component.html',
  styleUrls: ['./vcontact-list-page.component.scss'],
})
export class VContactListPageComponent extends BasePage implements OnInit {

  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  @ViewChild('virtualscroll', { static: true }) virtualscroll: IonVirtualScroll;


  public search_value;
  public search_on = false;

  constructor(injector: Injector, public storedcontacts: StoredContactsService) {
    super(injector)

    this.search_on = false;
    this.storedcontacts.getMyContacts(null, true, 0, false);
  }

  ngOnInit() { }

  setFocusOnSearch() {
    var self = this;
    this.search_on = true;
    setTimeout(() => {
      self.searchbar.setFocus();
    }, 500);

  }

  filterGlobal($event) {
    let val = this.search_value;
    this.storedcontacts.getMyContacts(val, true, 0, false).then(v => {
      // console.log(v);
    });

  }

  resetSearch() {
    this.search_value = null;
    this.storedcontacts.getMyContacts(null, true, 0, false)
  }

  doRefresh($event) {
    this.storedcontacts.getMyContacts(this.search_value, true, 0, false).then(v => {
      $event.complete();
    });
  }

  loadMoreContacts($event) {       
    this.storedcontacts.getNetworkContacts(this.search_value, 0, true).then(v => {
      $event.target.complete();
    });
  }

  goBack() {
    this.events.publish('contacts:popallpages', Date.now());
  }

  async presentPopover($event, item) {

    const _data = await this.popover.present($event, {
      pid: item,
      flag: "C"
    }, 'interface-style')

    const data = _data['data'];

    if (data == null) {
      return;
    }
    console.log(data);
    var item = data["pid"];

    switch (data["param"]) {
      case "C":
        this.utility.dialMyPhone(item.phone_number);
        break;
      case "G":
        item['contact_id'] = item['id']
        this.storedcontacts.createSelectedInContactGroupByItem([item]);
        break;
      case "E":
        this.storedcontacts.editFamily(item);
        break;
      case "F":
        this.storedcontacts.createSelectedInContactFavouritiesByItem([item]);
        break;
      case "D":
        this.storedcontacts.deleteContact(item);
        break;
      case "SP":
        this.storedcontacts.fetchEventsToSendPassTo(item)
        break;
      case "RP":
        this.storedcontacts.sendPassRequestToContact(item);
        break;
    }

  }

  plusHeaderButton(param) {
    if (param == "S") {
      this.storedcontacts.editFamily(null);
    } else {
      this.storedcontacts.syncMyContact();
    }
  }

  async syncContactsFromApi() {
    this.utility.showLoader();
    await this.storedcontacts.syncContactsFromApi(false);
    this.utility.hideLoader();

  }

}
