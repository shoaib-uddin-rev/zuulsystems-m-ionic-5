import { MypassesdetailPageComponent } from './../mypassesdetail-page/mypassesdetail-page.component';
import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-notifications-page',
  templateUrl: './notifications-page.component.html',
  styleUrls: ['./notifications-page.component.scss'],
})
export class NotificationsPageComponent extends BasePage implements OnInit {

  plist: any[] = [];
  avatar = this.users.avatar;
  user_events: any[] = [];
  offset = 0;
  public loading: boolean = true;
  public doneMarkAllRead:boolean = false;

  constructor(injector: Injector) { 
    super(injector);
    this.fetchNotifications();

  }

  ngOnInit() {}

  removeNotf(item){
    // console.log(item);
    this.spliceItem(item)
    this.network.removeNotification(item.id).then((response: any) =>{
      // console.log(response);
    }, err => {});

  }


  rejectNotf(item){

    this.spliceItem(item)
    var dataObj = {it_id: item}
    this.network.rejectPassRequest(dataObj).then((response: any) =>{
      this.removeNotf(item)
    });
  }

  acceptNotf(item, ease){
    // console.log(item, ease);
    this.spliceItem(item)

    var it = {it_id: item, ev_id: ease}
    this.network.acceptPassRequest(it).then((response: any) =>{
      this.removeNotf(item)
    });
  }

  createPass(item){
    this.modals.dismiss(item).then(() => {
      this.removeNotf(item)

      if(item.details.anonymous == 1){
        // console.log(item);
        var f = {};
        f['selected_contacts'] = [item.user_id];
        f['event_id'] = 15;
        f['event_name'] = "Instant Pass";
        f['pass_date'] = this.utility.formatDateTime(new Date);
        f['pass_validity'] = '5';
        f['pass_type'] = 1;
        f['visitor_type'] = 1;
        f['pr_id'] = item['pr_id']
        var __profile = this.users._user.profile
        f['description'] = this.utility.parseAddressFromProfile(__profile);
        this.network.createNewPass(f).then((res: any) =>{
          this.utility.presentSuccessToast("Instant pass sent successfully");
        }, error => {});


      }else{

        // console.log(item);
        this.events.publish('user:redirectToCreatePass', item);

      }

    });
  }

  sendAnonymousPassToUser(){

  }





  async showPass(item){
    // console.log(item);
    const data = await this.modals.present(MypassesdetailPageComponent, {item: item.pr_id, flag: "active"})
  }

  spliceItem(item){
    const index: number = this.plist.indexOf(item);
    if (index !== -1) {
        this.plist.splice(index, 1);
    }
  }

  fetchNotifications(showloader = true){

    return new Promise( resolve => {

      if(this.offset == -1){
        resolve();
      }else{

        this.loading = showloader;
        var params = {offset: this.offset};

        this.network.getOnePageNotifications(params).then((res: any) => {

          if(this.offset == 0){
            this.plist = res['notifications'];
          }else{
            this.plist = this.plist.concat(res['notifications']);
          }

          this.offset = res['offset'];
          this.loading = false;
          resolve();
        }, err => {})

      }


    })

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  convertSecondstoTime(giventime) {

    var t1 = new Date(giventime);
    var t2 = new Date();
    var dif = t1.getTime() - t2.getTime();

    var Seconds_from_T1_to_T2 = dif / 1000;
    return Math.round(Seconds_from_T1_to_T2);

  }

  markasread(item){
    this.network.markReadNotification({ ids: [item.id] }).then((response: any) =>{
      // console.log(response);
    }, err => {});
  }

  markAllAsRead(){

    var ids = [];
    this.plist.forEach(element => {
      element.status = 1;
      ids.push(element.id);
    });

    this.doneMarkAllRead = true;
    this.network.markReadNotification({ ids: ids, all: true }).then((response: any) =>{
      // console.log(response);
    }, err => {});

  }

  async doRefresh(refresher = null){
    this.offset = 0;
    this.plist = [];
    this.fetchNotifications();
    refresher.complete();
  }

  loadMore($event){

    this.fetchNotifications(false).then( v => {
      $event.complete();
    });


  }

}
