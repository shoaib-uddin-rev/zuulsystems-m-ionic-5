import { BasePage } from 'src/app/pages/base-page/base-page';
import { Component, OnInit, Injector, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-event-page',
  templateUrl: './create-event-page.component.html',
  styleUrls: ['./create-event-page.component.scss'],
})
export class CreateEventPageComponent extends BasePage implements OnInit {

  aForm: FormGroup;
  submitAttempt = false;
  @Input() event;

  constructor(injector: Injector) { 
    super(injector);

    this.setupForm();

    if(this.event){

      this.aForm.controls['event_name'].setValue(this.event['event_name']);
      this.aForm.controls['event_description'].setValue(this.event['event_description']);

    }
  }

  ngOnInit() {}

  setupForm(){

    this.aForm = this.formBuilder.group({
      event_name: ['', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
      event_description: ['']
    })

  }

  create(){

    var in_name = !this.aForm.controls.event_name.valid;

    if(in_name){
      this.utility.presentFailureToast("event name is required")
      return
    }

    this.submitAttempt = true;
    var formdata = this.aForm.value;
    this.network.addEventToList(formdata).then((res: any) =>{
      //this.utility.presentSuccessToast(res['message']);
      this.sqlite.setEventInDatabase(res["event"])
      this.closeModal({data: res["event"]});

    }, err => {});



  }

  update(){

    var in_name = !this.aForm.controls.event_name.valid;

    if(in_name){
      this.utility.presentFailureToast("group name is required")
      return
    }

    this.submitAttempt = true;
    var formdata = this.aForm.value;

    formdata['id'] = this.event.id;
    this.network.editEventToList(formdata).then((res: any) =>{
      //this.utility.presentSuccessToast(res['message']);
      this.sqlite.setEventInDatabase(res["event"])
      this.closeModal({data: res["event"]});

    }, err => {});

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

}
