import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GmapPageComponent } from './gmap-page.component';

describe('GmapPageComponent', () => {
  let component: GmapPageComponent;
  let fixture: ComponentFixture<GmapPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmapPageComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GmapPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
