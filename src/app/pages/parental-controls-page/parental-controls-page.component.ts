import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-parental-controls-page',
  templateUrl: './parental-controls-page.component.html',
  styleUrls: ['./parental-controls-page.component.scss'],
})
export class ParentalControlsPageComponent extends BasePage implements OnInit {


  item;
  user_id;

  aForm: FormGroup;
  akeys: any = [];

  constructor(injector: Injector) {
    super(injector);

    this.user_id = this.nav.getQueryParams().user_id;
    // console.log(this.item);
    this.setupForm();
    this.fetchParentalValues()

  }

  ngOnInit() { }

  setupForm() {

    this.aForm = this.formBuilder.group({
      notify_when_member_guest_arrive: [1, Validators.required],
      notify_when_member_send_pass_to_others: [1, Validators.required],
      notify_when_member_receive_a_pass: [1, Validators.required],
      notify_when_member_retract_a_pass: [1, Validators.required],
      notify_when_member_update_a_pass: [1, Validators.required],
      notify_when_pass_retracted_included_member: [1, Validators.required],
      notify_when_pass_updated_included_member: [1, Validators.required],
      notify_when_member_request_a_pass_with_offensive_words: [1, Validators.required],
      blacklist_member_to_receive_request_a_pass: [1, Validators.required],
      allow_parental_controls_to_member: [1, Validators.required],
    })

    const keys = Object.keys(this.aForm.controls);
    this.akeys = keys.map(x => {
      return {
        'key': x,
        'label': x.replace(/_/g, ' ')
      }
    })

  }

  fetchParentalValues() {

    this.network.getParentalControlOptions(this.user_id).then(data => {
      // console.log(data);

      const options = data['options'];

      // tslint:disable-next-line:forin
      for (var key in options) {

        // console.log(key);
        var val = options[key];
        key = key.toLowerCase();
        // console.log(key, val);
        if (this.aForm.controls[key]) {
          this.aForm.controls[key].setValue(val);
        }

      }

      //       Allow_parental_controls_to_member: 1
      // Blacklist_member_to_receive_request_a_pass: 1
      // Member_id: 1944
      // Notify_when_member_guest_arrive: 0
      // Notify_when_member_receive_a_pass: 0
      // Notify_when_member_request_a_pass_with_offensive_words: 1
      // Notify_when_member_retract_a_pass: 1
      // Notify_when_member_send_pass_to_others: 1
      // Notify_when_member_update_a_pass: 1
      // Notify_when_pass_retracted_included_member: 1
      // Notify_when_pass_updated_included_member: 1




    }, err => { })
  }

  saveSelection() {
    this.network.setParentalControlOptions(this.user_id, this.aForm.value).then(data => {
      this.utility.presentSuccessToast("Settings Saved");
      this.nav.pop();
    }, err => { })
  }

}
