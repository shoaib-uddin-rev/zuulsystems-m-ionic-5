import { Component, OnInit, Injector, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { VehicleServiceService } from 'src/app/services/vehicle-service.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-create-vehicle-page',
  templateUrl: './create-vehicle-page.component.html',
  styleUrls: ['./create-vehicle-page.component.scss'],
})
export class CreateVehiclePageComponent extends BasePage implements OnInit {


  _item;
  @Input() set item(value: any) {
    this._item = value;
    if(this.item){
      this.isUpdate = true;
      this.btnText = "Update";

      //console.log(this.contact_id);
      // this.getContactInfo(this.contact_id);
      this.fetchVehicleValues(this.item)
    }
    
  }

  get item(): any {
    return this._item;
  }





  is_image_edited = false;
  aForm: FormGroup;
  submitAttempt = false;
  isUpdate: boolean = false;
  btnText: string = "Create";
  hexcolor = "#FFFFFF";

  constructor(injector: Injector, public vehicleService: VehicleServiceService) { 
    super(injector)
    this.setupForm();

    
  }

  ngOnInit() {}

  fetchVehicleValues(item) {
    this.aForm.controls.licence_plate.setValue(item['licence_plate']);
    this.aForm.controls.make.setValue(item['make']);
    this.aForm.controls.model.setValue(item['model']);
    this.aForm.controls.year.setValue(item['year']);
    this.aForm.controls.color.setValue(item['color']);
    this.aForm.controls.image.setValue(item['image']);
  }

  setupForm(){

    this.aForm = this.formBuilder.group({
      licence_plate: ['', Validators.compose([Validators.required])],
      make:  [''],
      model: [''],
      year:  [''],
      color: [''],
      image: ['assets/imgs/upload.png']
    })

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  getImage() {
    this.utility.snapImage("profile").then(image => {
      let piImageBase64 = image as string;
      this.aForm.controls["image"].setValue(piImageBase64);
      this.is_image_edited = true;
    });
  }

  returnInvalidImage(){
    return !this.aForm.controls.image.valid || this.aForm.controls.image.value == 'assets/imgs/upload.png' || this.aForm.controls.image.value.includes('car_avatar')
  }

  async create(){


    // add validations
    var in_lp = !this.aForm.controls.licence_plate.valid
    var in_make = !this.aForm.controls.make.valid
    var in_model = !this.aForm.controls.model.valid
    var in_year = !this.aForm.controls.year.valid
    var in_color = !this.aForm.controls.color.valid
    var in_image = !this.aForm.controls.image.valid // || this.aForm.controls.image.value == 'assets/imgs/upload.png'

    if(in_image){ this.utility.presentFailureToast( "Please Upload Vehicle Image"); return }
    if(in_lp){ this.utility.presentFailureToast( "Please Enter Licence Plate"); return }
    if(in_make){ this.utility.presentFailureToast( "Please Enter Vehicle Make"); return }
    if(in_model){ this.utility.presentFailureToast( "Please Enter Vehicle Model"); return }
    if(in_year){ this.utility.presentFailureToast( "Please Enter Vehicle Year"); return }
    if(in_color){ this.utility.presentFailureToast( "Please Enter Vehicle Color"); return }


    var formdata = this.aForm.value;
    formdata['is_update'] = this.isUpdate;
    formdata['licence_plate'] = formdata['licence_plate'].toUpperCase();
    // console.log(formdata);

    if(this.is_image_edited == false){
      delete formdata['image']
    }

    if(this.isUpdate){
      let vehicle = await this.vehicleService.editVehicle(this.item['id'], formdata);
      if(vehicle){
        this.closeModal(vehicle);
      }
    }else{
      let vehicle = await this.vehicleService.addVehicle(formdata);
      if(vehicle){
        this.closeModal(vehicle);
      }
    }


  }

  setColor($event){
    // console.log("set color - ", $event);
    this.aForm.controls.color.setValue($event);
  }

  colorTouchStart($event){
    // console.log("touch start - ", $event);
  }

  colorTouchEnd($event){
    // console.log("touch end" , $event);
  }

}
