import { MyfamilymembersPageComponent } from './myfamilymembers-page/myfamilymembers-page.component';
import { ActivePageComponent } from './mypasses-page/active-page/active-page.component';
import { VGroupContactListComponent } from './vcontacts-page/vgroup-contact-list/vgroup-contact-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ManageVehiclePageComponent } from './manage-vehicle-page/manage-vehicle-page.component';
import { NewPassPageComponent } from './new-pass-page/new-pass-page.component';
import { PasscontactsComponent } from './new-pass-page/passcontacts/passcontacts.component';
import { PassendpageComponent } from './new-pass-page/passendpage/passendpage.component';
import { PassformComponent } from './new-pass-page/passform/passform.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { SettingsPageComponent } from './settings-page/settings-page.component';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { VContactsPageComponent } from './vcontacts-page/vcontacts-page.component';
import { MypassesPageComponent } from './mypasses-page/mypasses-page.component';
import { ArchivedPageComponent } from './mypasses-page/archived-page/archived-page.component';
import { SentPageComponent } from './mypasses-page/sent-page/sent-page.component';
import { VContactListPageComponent } from './vcontacts-page/vcontact-list-page/vcontact-list-page.component';
import { VGroupListPageComponent } from './vcontacts-page/vgroup-list-page/vgroup-list-page.component';
import { VFavoriteListPageComponent } from './vcontacts-page/vfavorite-list-page/vfavorite-list-page.component';
import { ParentalControlsPageComponent } from './parental-controls-page/parental-controls-page.component';
import { RequestAPassPageComponent } from './request-apass-page/request-apass-page.component';
import { ChoiceOptionsComponent } from './request-apass-page/choice-options/choice-options.component';
import { CommentOptionsComponent } from './request-apass-page/comment-options/comment-options.component';


const routes: Routes = [
    { path: 'splash', component: SplashPageComponent},
    { path: 'TutorialPage', component: TutorialPageComponent},
    { path: 'LoginPage', component: LoginPageComponent},
    { path: 'SignupPage', component: SignupComponent},
    { path: 'DashboardPage', component: DashboardPageComponent},
    { path: 'RegistrationPage', component: RegisterPageComponent},
    { path: 'SettingsPage', component: SettingsPageComponent},
    { path: 'VGroupContactListPage', component: VGroupContactListComponent},
    { 
      path: 'VContactsPage',
      component: VContactsPageComponent,
      children: [
        { path: 'VContactListPage', component: VContactListPageComponent},
        { path: 'VGroupListPage', component: VGroupListPageComponent},
        { path: 'VFavoriteListPage', component: VFavoriteListPageComponent},
        {
          path: '',
          redirectTo: 'VContactListPage',
          pathMatch: 'full'
        }
      ]
    },
    {
      path: 'NewPassPage',
      component: NewPassPageComponent,
      children: [
        { path: 'passform', component: PassformComponent},
        { path: 'passcontacts', component: PasscontactsComponent},
        { path: 'passendpage', component: PassendpageComponent},
        {
          path: '',
          redirectTo: 'passform',
          pathMatch: 'full'
        }
      ]
    },
    {
      path: 'MypassesPageComponent',
      component: MypassesPageComponent,
      children: [
        { path: 'ActivePage', component: ActivePageComponent},
        { path: 'ArchivedPage', component: ArchivedPageComponent},
        { path: 'SentPage', component: SentPageComponent},
        {
          path: '',
          redirectTo: 'ActivePage',
          pathMatch: 'full'
        }
      ]
    },
    { path: 'MyfamilymembersPage', component: MyfamilymembersPageComponent},
    { path: 'ParentalControlsPage', component: ParentalControlsPageComponent},
    {
      path: 'RequestAPassPageComponent',
      component: RequestAPassPageComponent,
      children: [
        { path: 'ChoiceOptionsComponent', component: ChoiceOptionsComponent},
        { path: 'CommentOptionsComponent', component: CommentOptionsComponent},
        {
          path: '',
          redirectTo: 'ChoiceOptionsComponent',
          pathMatch: 'full'
        }
      ]
    },
    
    {
        path: '',
        redirectTo: 'splash',
        pathMatch: 'full'
    },

];  


@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class PagesRoutingModule { }