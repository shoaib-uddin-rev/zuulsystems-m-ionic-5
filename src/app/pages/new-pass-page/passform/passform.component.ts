import { Component, Injector, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonDatetime } from '@ionic/angular';
import { PassServiceService } from 'src/app/services/pass-service.service';
import { BasePage } from '../../base-page/base-page';
import { GmapPageComponent } from '../../gmap-page/gmap-page.component';
import { OtherEventsPageComponent } from '../../other-events-page/other-events-page.component';

@Component({
  selector: 'app-passform',
  templateUrl: './passform.component.html',
  styleUrls: ['./passform.component.scss'],
})
export class PassformComponent extends BasePage implements OnInit {

  _item: any = {};
  user: any;
  @Input() edit = false;
  @Input()
  get item(): any { return this._item; }
  set item(item: any) {
    this._item = item;
  }


  aForm: FormGroup;
  @ViewChild('enddatetime') sTime: IonDatetime;

  submitAttempt = false;
  user_events: any[];
  date_of_pass_mdy;
  end_date_of_pass_mdy;
  selected_event_name: string = "";
  dateNowIsoString; 
  dateFutureIsoString;
  custom_date_lbl = 'Custom'
  custom_date_value_lbl = 'xxx'

  
  constructor(injector: Injector, public passservice: PassServiceService) { 
    super(injector);

    this.setupForm();
    this.setDateValidationIn();
    

  }

  nextAvailable(){

    return !this.aForm.controls.pass_date.valid ||
        !this.aForm.controls.pass_validity.valid ||
        !this.aForm.controls.pass_type.valid ||
        !this.aForm.controls.description.valid;

  }

  ngOnInit() {}

  variablesSet(){

    // if (this.edit == true) {
    //   this.setEditFormValues()

    // } else
    //   if (this.rap == true) {
    //     this.rapitem = this.item;
    //     this.isGroup = this.group;
    //     if (this.isGroup == true) {

    //       this.selected_contacts = [];

    //     } else {
    //       this.selected_contacts = [this.rapitem.user_id]
    //     }

    //     // console.log(this.selected_contacts);

    //   } else {

    //     this.isContact = this.contact;
    //     if (this.isContact == true) {
    //       let item = this.item;
    //       this.selected_contacts.push(item.user_id);
    //     }

    //     this.isGroup = this.group;
    //     if (this.isGroup == true) {
    //       this.selected_contacts = [];
    //     }

    //   }
  }

  setDateValidationIn() {

    this.dateNowIsoString = new Date().toISOString();

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth();
    var day = d.getDate();
    var c = new Date(year + 100, 12, 31).toISOString();
    // console.log(c);

    this.dateFutureIsoString = c;

  }

  setEndTimeAfterValidityChange(ev) {
    let v = this.aForm.controls['pass_date'].value;
    if (v == null || v == '') {
      return
    };


    let pass_validity = this.aForm.controls['pass_validity'].value;


    if (pass_validity == 'xxx') {

      var self = this;
      this.sTime.open();

    } else {
      this.setFutureDate();
    }


  }

  async setupForm() {

    let tempdate = new Date();
    let myDate = new Date(tempdate.getTime() - tempdate.getTimezoneOffset() * 60000).toISOString();
    // var re = /\S+@\S+\.\S+/;
    this.aForm = this.formBuilder.group({
      pass_date: [myDate, Validators.compose([Validators.required])],
      pass_validity: ['24', Validators.compose([Validators.required])],
      pass_type: ['1', Validators.compose([Validators.required])],
      visitor_type: ['1', Validators.compose([Validators.required])],
      description: ['Default Location not found', Validators.compose([Validators.required])],
      event_id: ['', Validators.compose([Validators.required])],
      pass_image: ['', Validators.compose([Validators.required])],
      event_name: [''],
      mycontacts: ['contacts'],
      selected_contacts: ['false'],
      lat: [''],
      lng: [''],
      searchTerm: [''],
      vendor_id: [''],
      vendor_name: [''],
      pass_end_date: [''],

    });

    this.setFutureDate();

    await this.getUserEvents();

    await this.getuser();

    if (this.edit != true) {
      var __profile = this.user.profile
      console.log(this.user, __profile);
      var address = this.utility.parseAddressFromProfile(__profile);
      this.aForm.controls['description'].setValue(address);
    }

  }

  getuser() {
    return new Promise(async resolve => {
      this.user = await this.sqlite.getActiveUser();
      resolve();
    })

  }

  async getUserEvents() {

    this.passservice.getUserEvent().then(f => {
      // console.log(f);
      this.aForm.controls['event_id'].setValue(f["event_id"]);
      this.aForm.controls['event_name'].setValue(f["event_name"]);
      this.selected_event_name = f["event_name"];
    })

  }

  setFutureDate() {
    var self = this;
    this.passservice.getFutureDateISO(this.aForm.controls['pass_date'].value, this.aForm.controls['pass_validity'].value).then(v => {
      self.end_date_of_pass_mdy = v
      console.log(v)
      self.aForm.controls.pass_end_date.setValue(v);
    })
  }

  setEditFormValues() {

    let _date = Date.parse(this.item.pass_start_date)
    console.log(this.item.description);
    let iso = new Date(_date).toISOString();
    this.aForm.controls['pass_date'].setValue(iso);
    // this.aForm.controls['pass_date'].setValue(new Date(_date));
    this.date_of_pass_mdy = this.utility.formatAMPM(_date);
    this.aForm.controls.pass_validity.setValue(this.item.pass_validity);
    this.aForm.controls.pass_type.setValue(this.item.pass_type);
    this.aForm.controls.visitor_type.setValue(this.item.visitor_type);
    this.aForm.controls.description.setValue(this.item.description);
    this.aForm.controls.event_id.setValue(this.item.event_id);
    this.aForm.controls.pass_image.setValue(this.item.pass_image);
    this.aForm.controls.event_name.setValue(this.item.get_event.event_name);
    this.selected_event_name = this.item.get_event.event_name;

    this.aForm.controls.lat.setValue(this.item.latitude);
    this.aForm.controls.lng.setValue(this.item.longitude);
    this.setFutureDate();

  }

  async openEventSelection() {
    let _data = { items: this.user_events };
    const data = await this.modals.present(OtherEventsPageComponent, _data);
    let event = data["data"]
    if (data.data.data != 'A') {
      this.selected_event_name = event.event_name;
      this.aForm.controls['event_id'].setValue(event.id);
      this.aForm.controls['event_name'].setValue(event.event_name);
    }

  }

  async setDate() {

    var self = this;

    const date = await this.utility.showDatePicker(new Date(), 'datetime');
    if (!date) { return }
    this.aForm.controls['pass_date'].setValue(this.utility.formatDateTime(date));
    this.date_of_pass_mdy = this.utility.formatAMPM(date);
    self.setFutureDate()
  }

  

  async openLocation() {
    var address = this.aForm.controls['description'].value;
    const _data = await this.modals.present(GmapPageComponent, { myAddress: address });
    const data = _data.data;
    console.log(data);
    if (data) {
      this.aForm.controls['lat'].setValue(data.lat);
      this.aForm.controls['lng'].setValue(data.lng);
      this.aForm.controls['description'].setValue(data.address);
    }
  }

  goToNext(num){

    // set data to view 
    this.events.publish("newpass:setformdata", this.aForm.value);
    this.events.publish("newpass:slidetoNext", num);
  }

  closemodal(){
    this.events.publish("newpass:closemodal", {});
  }

}
