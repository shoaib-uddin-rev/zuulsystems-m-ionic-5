import { PassServiceService } from './../../services/pass-service.service';
import { Component, OnInit, Injector, ViewChild, Input, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonSlides, IonDatetime } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { OtherEventsPageComponent } from '../other-events-page/other-events-page.component';
import { GmapPageComponent } from '../gmap-page/gmap-page.component';
import { StoredContactsService } from 'src/app/services/stored-contacts.service';

@Component({
  selector: 'app-new-pass-page',
  templateUrl: './new-pass-page.component.html',
  styleUrls: ['./new-pass-page.component.scss'],
})
export class NewPassPageComponent extends BasePage implements OnInit, AfterViewInit {


  @ViewChild('slides', { static: true }) slides: IonSlides;
  

  _item: any = {};
  heading1 = 'Pass Has Been Sent';
  heading2 = 'A pass invite along with a unique invitation code has been sent to all your contacts'
  submitAttempt = false;
  formdata: any;
  user;
  
  @Input() edit: boolean = false;
  @Input() rap: boolean = false;
  @Input() rapitem: any = null;
  @Input() isGroup: boolean = false;
  @Input() group: boolean = false;
  @Input() isContact: boolean = true;
  @Input() contact: boolean = true;
  reminderObject: any;
  isSelectedUser = false;
  selected_contacts = [];

  @Input()
  get item(): any { return this._item; }
  set item(item: any) {
    this._item = item;
  }

  constructor(injector: Injector, public passservice: PassServiceService) {
    super(injector);
    
    this.events.subscribe("newpass:slidetoNext", this.goToNext.bind(this));
    this.events.subscribe("newpass:slidetoPrev", this.goToPre.bind(this));
    this.events.subscribe("newpass:closemodal", this.closePassModal.bind(this));
    this.events.subscribe("newpass:setformdata", this.setFormData.bind(this));
    this.events.subscribe("newpass:setselectedcontacts", this.setSelectedContacts.bind(this));
    this.events.subscribe("newpass:addareminder", this.addareminder.bind(this));
    
    this.getuser()
  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.slides.lockSwipes(true);
  }

  getuser(){
    return new Promise( async resolve => {
      this.user = await this.sqlite.getActiveUser();
      resolve();
    })

  }

  setFormData(data){
    this.formdata = data;
    console.log(this.formdata);
  }

  closePassModal(data) {
    this.modals.dismiss(data);
  }

  goToPre() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  goToNext(num){

    if(!this.validateSlide(num)){
      return;
    }


    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
  }

  setSelectedContacts(contacts){
    this.selected_contacts = contacts;
    //

    // add processing
    this.goToNext(2);

  }

  addareminder(){

    const date = this.formdata['pass_date'];
    let end_date_of_pass_mdy = this.utility.formatDateTime(date);


    const detailObj = {
      event_name: this.reminderObject.event_name,
      description: this.reminderObject.description,
      notes: "",
      startDate: Date.parse(this.reminderObject.pass_date),
      endDate: Date.parse(end_date_of_pass_mdy)
    }
    this.utility.addaremondertodate(detailObj);
  }

  validateSlide(num): boolean {


    console.log(this.edit, parseInt(this.formdata['pass_type']), this.rap, this.isGroup, this.isContact )
    var formdata = this.formdata as any;

    if (formdata['pass_validity'] == "xxx") {
      if (!this.calculateDateDifference(this.formdata['pass_end_date'])) {
        return;
      };
    }

    formdata["latitude"] = formdata["lat"];
    formdata["longitude"] = formdata["lng"];
    formdata["longitude"] = formdata["lng"];

    if (num == 1) {

      var in_evid = formdata['event_id'];
      var in_evname = formdata['event_name'];

      if (in_evid == '0' && in_evname == '') {
        this.utility.presentFailureToast("Please Enter Event Name");
        return
      }

      if (!in_evid) {
        in_evid = 14;
      }

      this.utility.setKey("event_id", in_evid);

      if (parseInt(this.formdata['pass_type']) == 3) {

        this.heading1 = 'Pass Created';
        this.heading2 = 'Pass has been sent to self';
        this.selected_contacts = [];
        this.selected_contacts.push(this.user.id);

        formdata["selected_contacts"] = this.selected_contacts;
        this.create(formdata);

      } else {

        if (this.edit == true) {

          formdata['edit'] = this.edit;
          formdata['id'] = this.item.id;

          this.create(formdata);

        }
        else
          if (this.rap == true) {

            formdata['rap'] = this.rap;
            formdata['rapitem'] = this.rapitem;

            formdata["selected_contacts"] = [this.rapitem.user_id];
            this.create(formdata);
          }
          else if (this.isGroup) {

            formdata["selected_contacts"] = [];
            let item = this.item;
            formdata['group_id'] = item.id;
            this.create(formdata);
          }
          else {

            if (this.isContact == true) {
              formdata["selected_contacts"] = this.selected_contacts;
              this.create(formdata);
            } else {
              this.slides.lockSwipes(false);
              this.slides.slideTo(num, 500);
              this.slides.lockSwipes(true);
            }

          }

      }


    } else

      if (num == 2) {

        formdata["selected_contacts"] = this.selected_contacts;
        console.log(formdata["selected_contacts"].length)
        if (formdata["selected_contacts"].length > 1 && this.formdata['pass_type'].value == 2) {
          this.utility.presentFailureToast("only one contact can be selected");
        } else {
          this.create(formdata)
        }

      } else {
        this.slides.lockSwipes(false);
        this.slides.slideTo(num, 500);
        this.slides.lockSwipes(true);
      }

  }

  create(formdata) {
    this.reminderObject = formdata;

    console.log(formdata);
    this.passservice.createPass(formdata).then(v => {
      this.goToNext(3);
    })
  }

  calculateDateDifference(end_date) {

    let start_date = this.formdata['pass_date'].value;
    // let end_date = this.end_date_of_pass_mdy

    let s = new Date(start_date.replace(" ", "T"));
    let e = new Date(end_date.replace(" ", "T"));

    let hours = this.utility.diffInHours(s, e);
    console.log(hours);
    if (hours <= 0) {
      this.formdata['pass_validity'].setValue("24");
      this.utility.presentFailureToast("End Date Must Be larger then Start Date");
      return false;
    }

    this.formdata['pass_validity'].setValue(hours);

    return true;

  }

  // addareminder() {
  //   const detailObj = {
  //     event_name: this.reminderObject.event_name,
  //     description: this.reminderObject.description,
  //     notes: "",
  //     startDate: Date.parse(this.reminderObject.pass_date),
  //     endDate: Date.parse(this.end_date_of_pass_mdy)
  //   }
  //   this.utility.addaremondertodate(detailObj);
  // }

  finishregister() {
    this.nav.pop().then(() => {
      this.events.publish('pass:tosenttab');
    });
  }

  

  // setFilteredItems(flag) {
  //   var self = this;
  //   var searchTerm = this.formdata['searchTerm'].value
  //   this.phone_contacts = this.phone_contacts_o.filter((item) => {
  //     return (item.phone_number != null && item.phone_number.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)
  //       || (item.display_name != null && item.display_name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)
  //   });
  // }

  // plusHeaderButton(param) {
  //   this.passservice.plusHeaderButton(param).then(v => {

  //     this.selected_contacts.forEach(c => {
  //       if (!this.selected_contacts.includes(v["id"])) {
  //         this.selected_contacts.push(c.id);
  //       }
  //     })
  //     this.selected_contacts = this.selected_contacts.concat(v);
  //   })
  // }


}
