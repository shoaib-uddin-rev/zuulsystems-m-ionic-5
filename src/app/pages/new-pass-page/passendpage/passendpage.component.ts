import { Component, OnInit, Input, Injector } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-passendpage',
  templateUrl: './passendpage.component.html',
  styleUrls: ['./passendpage.component.scss'],
})
export class PassendpageComponent extends BasePage implements OnInit {

  @Input() heading1;
  constructor(injector: Injector) { 
    super(injector)
  }

  ngOnInit() {}

  addareminder(){
    this.events.publish("newpass:addareminder", this.addareminder.bind(this));
  }

  finishregister(){
    console.log("re");
    this.events.publish('newpass:closemodal');
  }

}
