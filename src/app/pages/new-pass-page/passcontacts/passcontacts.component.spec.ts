import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PasscontactsComponent } from './passcontacts.component';

describe('PasscontactsComponent', () => {
  let component: PasscontactsComponent;
  let fixture: ComponentFixture<PasscontactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasscontactsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PasscontactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
