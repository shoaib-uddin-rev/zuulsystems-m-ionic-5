import { ContactSearchListComponent } from './../../components/contact-search-list/contact-search-list.component';
import { NgModule } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { NewPassPageComponent } from './new-pass-page.component';
import { PassformComponent } from './passform/passform.component';
import { PassendpageComponent } from './passendpage/passendpage.component';
import { PasscontactsComponent } from './passcontacts/passcontacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
        declarations: [
                NewPassPageComponent,
                PassformComponent,
                PassendpageComponent,
                PasscontactsComponent,
        ],
        imports: [
                CommonModule,
                IonicModule,
                FormsModule,
                ReactiveFormsModule


        ],
        exports: [
                NewPassPageComponent
        ],
        providers: [
                Location
        ]
})
export class NewPassPageComponentModule { }