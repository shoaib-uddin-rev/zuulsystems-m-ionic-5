import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-parental-logs-page',
  templateUrl: './parental-logs-page.component.html',
  styleUrls: ['./parental-logs-page.component.scss'],
})
export class ParentalLogsPageComponent extends BasePage implements OnInit {

  plist: any[] = [];
  offset = 0;
  public loading: boolean = true;
  public doneMarkAllRead:boolean = false;

  constructor(injector: Injector) {
    super(injector);
    this.fetchNotifications();
  }

  ngOnInit() {}

  fetchNotifications(showloader = true){


    return new Promise( resolve => {


      if(this.offset == -1){
        resolve();
      }else{

        this.loading = showloader;
        var params = {offset: this.offset};

        this.network.getParentalNotifications(params).then((res: any) => {

          console.log(res);
          if(this.offset == 0){
            this.plist = res['notifications'];
          }else{
            this.plist = this.plist.concat(res['notifications']);
          }

          this.offset = res['offset'];
          this.loading = false;
          resolve();
        }, err => {})

      }


    })

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ParentalLogsPage');
  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  markasread(item){

    this.network.markReadPCNotification({ ids: [item.id] } ).then((response: any) =>{
      // console.log(response);
    }, err => {});
  }

  markAllAsRead(){
    var ids = [];
    this.plist.forEach(element => {
      element.status = 1;
      ids.push(element.id);
    });

    this.doneMarkAllRead = true;
    this.network.markReadPCNotification({ ids: ids, all: true } ).then((response: any) =>{
      // console.log(response);
    }, err => {});

  }

  async doRefresh(refresher = null){
    this.offset = 0;
    this.plist = [];
    this.fetchNotifications();
    refresher.complete();
  }

  loadMore($event){

    this.fetchNotifications(false).then( v => {
      $event.complete();
    });


  }

}
