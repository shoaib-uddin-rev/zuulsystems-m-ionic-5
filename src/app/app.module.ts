import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FCM } from '@ionic-native/fcm/ngx';
import { NgxPubSubModule, NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { Contacts} from '@ionic-native/contacts/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SqliteService } from './services/sqlite.service';
import { StorageService } from './services/basic/storage.service';
import { ApiService } from './services/api.service';
import { NetworkService } from './services/network.service';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PagesModule } from './pages/pages.module';
import { EventsService } from './services/events.service';
import { TutorialCardsComponentModule } from './components/tutorial-cards/tutorial-cards-component.module';
import { InterceptorService } from './services/interceptor.service';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
   
    HttpClientModule,
    PagesModule,
    NgxPubSubModule,
    ImageCropperModule,
    NgxQRCodeModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    FCM,
    SQLite,
    DatePicker,
    Camera,
    ImagePicker,
    Base64,
    File,
    Crop,
    Contacts,
    Geolocation,
    NativeGeocoder,
    Calendar,
    LaunchNavigator,
    CallNumber,
    InAppBrowser,
    Keyboard,
    OpenNativeSettings,
    AndroidPermissions,
    NativeStorage,
    // custom providers
    NgxPubSubService,
    EventsService,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },















  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
