import { PopoverController } from '@ionic/angular';
import { Component, OnInit, Injector, Input, AfterViewInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BaseComponent } from '../base-component/base-component';

@Component({
  selector: 'app-contactpopover-page',
  templateUrl: './contactpopover-page.component.html',
  styleUrls: ['./contactpopover-page.component.scss'],
})
export class ContactpopoverPageComponent extends BaseComponent implements OnInit, AfterViewInit {

  @Input() flag = 'C';
  @Input() count = 0;
  user: any;
  sw_users: any = [];
  // two flags for family member dropdown
  can_manage_family: boolean = false;
  can_send_passes: boolean = false;
  // can_request_passes: boolean = false;
  allow_parental_control: boolean = false;
  lock_head_of_family: boolean = false;
  isHeadOfFamily: boolean = false;
  suspandAccount: boolean = false;
  disableCreatePass: boolean = true;
  canBeResident: boolean = false;

  pc_unread;
  zuul_unread;
  loader = false;


  _pid: any;

  get pid(): any{
    return this._pid;
  }

  @Input() set pid(value: any) {
    this._pid = value;
    // console.log(value);
    // if(value){
    //   this.initialize();
    // }
    
  };

  constructor(injector: Injector, public popoverCtrl: PopoverController) { 
    super(injector)
    

  }
  ngAfterViewInit(): void {
    this.initialize();
  }

  ngOnInit() {}

  async initialize(){

    console.log(this.pid);
    this.loader = true;

    this.user = await this.sqlite.getActiveUser();
    if(this.user){
      // console.log(this.user);
      this.can_send_passes = ( this.user["can_send_passes"] == 1 );
      this.suspandAccount = (parseInt(this.user['suspand']) == 1);
    }



    if(this.flag == 'CF') {

      var head_of_family: boolean = !!parseInt(this.user.head_of_family);
      this.isHeadOfFamily = head_of_family;
      this.can_manage_family = !!parseInt(this.user.can_manage_family) || head_of_family;
      this.can_send_passes = await this.users.canSendPasses(this.user);
      this.allow_parental_control = !!parseInt(this.user.allow_parental_control) || head_of_family == true

      var admin = this.pid;
      this.lock_head_of_family = !!parseInt(admin.head_of_family)

    }

    if(this.flag == 'G'){
      this.can_send_passes = this.count > 0;
    }

    if(this.flag == 'C' || this.flag == 'F'){

      // var c = this.user.community;
      // var h = this.user.house;
      // var _c = this.pid.its_user['community']
      // var _h = this.pid.its_user['house']
      // // console.log(this.pid);
      // // console.log(this.user);
      // if(c == _c && h == _h){
      //   this.lock_head_of_family = !!parseInt(this.pid.its_user.head_of_family)
      //   //this.lock_head_of_family = true;
      // }

      // // console.log("K", this.user);

      // this.can_request_passes = !!parseInt(this.pid.its_user.can_send_passes) || !!parseInt(this.pid.its_user.head_of_family);
      // console.log(this.can_request_passes);

    }

    if(this.flag == 'SW'){

      this.sqlite.getAllRecords().then( data => {
        console.log(data);
        this.sw_users = data;
        this.loader = false;
      })

    }

    if(this.flag == "PCN"){
      this.loader = true;
      // fetch unread count from api
      this.network.getNotificationsCount().then( data => {
        // console.log(data);
        let _data = data['data'];
        this.pc_unread = _data['pc_unread'];
        this.zuul_unread = _data['zuul_unread'];
        this.loader = false;
      })
    }
  }

  close(param) {
    this.popoverCtrl.dismiss({pid: this.pid, param: param });
  }

  removeUser(item){
    // remove user from storage
    this.sqlite.removeLoginFromLocal(item['id']).then( data => {
      this.sw_users = data;
      if (parseInt(item.active) == 1) {
        this.events.publish('user:logout');
      }
    })
  }

  permissionsUpdate(data){
    this.pid[data['flag']] = data['value'];
    console.log(data);
    this.events.publish('permissions:update', data )
  }

}
