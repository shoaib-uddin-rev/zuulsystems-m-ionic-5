import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { IonItemSliding} from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';

@Component({
  selector: 'app-pass-item',
  templateUrl: './pass-item.component.html',
  styleUrls: ['./pass-item.component.scss'],
})
export class PassItemComponent implements OnInit {

  _item: any;

  bgcolor = 'green-background'; // green-background, gray-background
  get item(): any{
    return this._item;
  }

  @Input() set item(value: any) {
    this._item = value;
    console.log(value);
  };

  @Input('seeingOthersPass') seeingOthersPass = false;
  @Output('openPass') openPass: EventEmitter<any> = new EventEmitter<any>();
  @Output('openDirection') openDirection: EventEmitter<any> = new EventEmitter<any>();
  @Output('removePass') removePass: EventEmitter<any> = new EventEmitter<any>();
  @Output('editPass') editPass: EventEmitter<any> = new EventEmitter<any>();
  @Output('showHistory') showHistory: EventEmitter<any> = new EventEmitter<any>();
  @Output('showCheckbox') showCheckbox: EventEmitter<any> = new EventEmitter<any>();
  @Output('showGuestLogs') showGuestLogs: EventEmitter<any> = new EventEmitter<any>();


  constructor(public events: EventsService) { }

  ngOnInit() {}

  // archieved functions
  public open(itemSlide: IonItemSliding) {

    itemSlide.open('end');

    // reproduce the slide on the click
    // if(!this.seeingOthersPass &&  this.item.page != 'scanned'){
    //   itemSlide..setElementClass("active-sliding", true);
    //   itemSlide.setElementClass("active-slide", true);
    //   itemSlide.setElementClass("active-options-right", true);
    //   item.setElementStyle("transform", "translate3d(-200px, 0px, 0px)");
    // }
  }

  passDetails(item){
    let obj = {
      id: item['qrcode_id']
    };
    this.openPass.emit(obj);
  }

  getDirection(item){
    console.log(item.description)
    this.openDirection.emit(item.description);
  }

  deletePass(item){
    this.removePass.emit(item);
  }

  updatePass(item){
    this.editPass.emit(item);
  }

  btnShowHistory($event, item){
    this.showHistory.emit({event: $event, item: item })
  }

  btnShowCheckbox($event, item){
    this.showCheckbox.emit({event: $event, item: item })
  }

  btnShowGuestLogs($event, item){
    this.showGuestLogs.emit({event: $event, item: item })
  }


}
