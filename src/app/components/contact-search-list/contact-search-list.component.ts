import { StoredContactsService } from 'src/app/services/stored-contacts.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild, Injector } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { BaseComponent } from '../base-component/base-component';

@Component({
  selector: 'app-contact-search-list',
  templateUrl: './contact-search-list.component.html',
  styleUrls: ['./contact-search-list.component.scss'],
})
export class ContactSearchListComponent extends BaseComponent implements OnInit {



  group: any = {};
  @ViewChild('searchbar', {static: true}) searchbar:IonSearchbar;
  search_value;
  search_on = false;

  @Input('singleSelection') singleSelection: boolean = false;
  selected_contacts = [];

  public offset = 0;
  public contact_list: any[] = [];
  @Output('next') next: EventEmitter<any> = new EventEmitter<any>();

  constructor(injector: Injector, public storedcontacts: StoredContactsService) { 
    super(injector)
    this.getContactList(null, 0, false, false);
  }

  ngOnInit() {
    
  }

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }


  filterGlobal($event){
    let val = this.search_value;
    // console.log(val);
    this.getContactList(val, this.offset,  false, false);

  }


  resetSearch(){
    this.getContactList(null, 0, false, false);
  }


  async getContactList(search = null, offset, loader = true, is_concat = true){

    // console.log(this.offset);
    return new Promise( resolve => {
      this.sqlite.getContacts(search, offset, 0, loader ).then((res: any) => {

        console.log(res);
        if(is_concat){
          this.offset = res['offset'] as number;
          this.contact_list = this.contact_list.concat(res['contact_list']);
        }else{
          this.offset = res['offset'] as number;
          this.contact_list = res['contact_list'];
        }

        resolve(this.contact_list);
      }, error => {
        resolve(this.contact_list);
      });
    });
  }

  doRefresh($event){
    this.offset = 0;
    this.getContactList(this.search_value, this.offset, false, false).then( v => {
      this.selected_contacts = [];
      $event.target.complete();
    });
  }

  loadMoreContacts($event){
    this.getContactList(this.search_value, this.offset, false, true).then( v => {
      $event.target.complete();
    });
  }

  async deleteContactFromTemp(item){

      await this.sqlite.removeContactFromTemporary(item.id);
      var elementPos = this.contact_list.map(function (x) { return x.id; }).indexOf(item.id);
      this.contact_list.splice(elementPos, 1);

  }

  plusHeaderButton(param) {
    if (param == "S") {
      this.storedcontacts.editFamily(null).then( async v => {
        let contact_ids = "( " + v["id"] + " )"
        await this.sqlite.setTemporaryContactsInDatabase(contact_ids);
        v["new"] = true;
        this.contact_list.unshift(v);
      });
    }else if(param == "R"){
      // reset contact list
      this.storedcontacts.removeAllTempContacts().then( v => {
        this.resetSearch()
      })
    } else {
      this.storedcontacts.syncToTempContact().then( v => {
        this.resetSearch()
      });
    }
  }

  async goToNext(){
    let contacts = await this.getFilteredContacts();
    this.next.emit(contacts);
  }

  getFilteredContacts(){
    return new Promise( resolve => {
      this.sqlite.getContacts(null, 0, 0, false, this.selected_contacts ).then((res: any) => {
        // console.log(res);
        res['contact_list'];
        resolve(res['contact_list']);
      }, error => {
        resolve([]);
      });
    });
  }

  // presentPopover(myEvent, item) {
  //   let popover = this.popoverCtrl.create(ContactpopoverPage, {
  //     id: item,
  //     flag: "NPC"
  //   });
  //   popover.present({
  //     ev: myEvent
  //   });

  //   popover.onDidDismiss(data => {
  //     // console.log(data);
  //     if (data == null) {
  //       return;
  //     }
  //     var item = data["pid"];
  //     switch (data["param"]) {
  //       case "A":
  //         this.storedcontacts.syncToTempContact().then( v => {
  //           this.resetSearch()
  //         });
  //         break;
  //       case "N":
  //         this.storedcontacts.editFamily(null).then( async v => {
  //           let contact_ids = "( " + v["id"] + " )"
  //           await this.sqlite.setTemporaryContactsInDatabase(contact_ids);
  //           v["new"] = true;
  //           this.contact_list.unshift(v);
  //         });
  //         break;
  //       case "C":
  //         // reset contact list
  //         this.storedcontacts.removeAllTempContacts().then( v => {
  //           this.resetSearch()
  //         })
  //         break;
  //     }
  //   });
  // }

  isSelectedContact(id){
    return this.selected_contacts.some(el => el.id === id);
  }

  addSelectedContact($event){

    let check = $event.checked;
    let id = $event.id;
    // console.log(check, id);
    if(check == true){

      if(this.singleSelection){
        this.selected_contacts = [];
        this.selected_contacts.push(id);
        this.events.publish('contact-list:check-update', {id: id, includes: this.selected_contacts.includes(id) })
      }else{
        if(!this.selected_contacts.includes(id)){
          this.selected_contacts.push(id);
        }
      }




    }else{

        var index = this.selected_contacts.indexOf(id);

        if (index > -1) {
          this.selected_contacts.splice(index, 1);
        }

    }


    // console.log(this.selected_contacts);

  }

}
