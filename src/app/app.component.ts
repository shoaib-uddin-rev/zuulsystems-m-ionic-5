import { UserService } from './services/user.service';
import { Component } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { EventsService } from './services/events.service';
import { SqliteService } from './services/sqlite.service';
import { UtilityService } from './services/utility.service';
import { PermissionsService } from './services/permissions.service';
import { AudioService } from './services/audio.service';
import { NetworkService } from './services/network.service';
import { NavService } from './services/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  canBeResident: boolean = false;
  canShowSettings: boolean = true;
  isEmailVerificationPending: boolean = false;
  user_role_id = -1;

  pages: any[] = [
    { title: 'Become Resident', component: 'CodeVerificationPage' },
    { title: 'Manage Family', component: 'MyfamilymembersPage' },
    { title: 'Settings', component: 'SettingsPage' },
    { title: 'Edit Profile', component: 'RegistrationPage' },
    { title: 'Logout', method: 'logout' },
    { title: 'Activity Logs', component: 'ActivityLogsPage' },
    { title: 'Zuul Key', component: 'ZKeyPage' },
    { title: 'Manage Vehicles', component: 'VManageVehiclesPage' },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    public menuCtrl: MenuController,
    private events: EventsService,
    private sqlite: SqliteService,
    public userService: UserService,
    public utilityProvider: UtilityService,
    public users: UserService,
    // public permission: PermissionsService,
    // private storage: Storage,
    public audio: AudioService,
    public network: NetworkService,
    public nav: NavService
  ) {
    platform.ready().then(() => {

      menuCtrl.enable(false, 'authenticated');
      statusBar.styleDefault();
      if (platform.is('cordova') || platform.is('ios') || platform.is('android')) {
        this.initializeApp();

      }
    });

    // assign events
    this.assignEvents();
  }

  initializeApp() {

    this.platform.ready().then( () => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.audio.preload();
      // await this.sqlite.initialize();
      this.setupFMC();
      this.initializeCountries();
    });

  }

  initializeCountries(){
    this.sqlite.getActiveUserId
  }

  setupFMC() {

    this.fcm.subscribeToTopic('all');
    this.fcm.onNotification().subscribe(data => {
      if (!data.wasTapped) {
        this.audio.play("");
        if (data['showalert'] != null) {
          this.events.publish('user:shownotificationalert', data);
        } else {
          this.events.publish('user:shownotification', data);
        }
      };
    })
    this.fcm.onTokenRefresh().subscribe(token => {
      this.sqlite.setFcmToken(token);
      this.events.publish('user:settokentoserver');
    });

  }

  async getFCMToken(){
    return new Promise( resolve => {
      this.fcm.getToken().then( v => resolve(v), (err) => { console.log(err); resolve(null) }).catch( v => resolve(null))
    })
  }

  private assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
    this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    this.events.subscribe('user:settokentoserver', this.setTokenToServer.bind(this));
    this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
    this.events.subscribe('user:setcontactstodatabase', this.setContacts.bind(this));

  }

  login(user) {

    console.log(user);

    var showelcome = user['showelcome']
    var email_data = {
      email: user.email,
      password: user.password,
    }

    var phone_data = {
      phone_number: user.phone_number,
      password: user.password,
      register_with_phonenumber: true
    }

    var data = (user.register_with_phonenumber == true) ? phone_data : email_data

    this.network.login(data).then(async res => {
      console.log(res);
      var user = res.user;
      if (user.is_guard == "1") {
        this.utilityProvider.presentToast("Guard / Security Personnal can't login here");
        this.nav.setRoot('LoginPage');
        return;
      }

      this.menuCtrl.enable(true, 'authenticated');
      const token = res.success.token;

      user['token'] = token;
      user['active'] = 1;

      await this.processUserData(user, showelcome);
    }, err => { })

  }

  logout() {
    this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    this.nav.setRoot('1/TutorialPage');
  }

  private getUser() {

    this.network.getUser().then(async (user: any) => {

      const _user = user.success;
      if (_user) {
        this.processUserData(_user, false);
      } else {
        // redirect to steps
        this.nav.push('Error404Page',
        { 
          afterLogin: true,
          animate: true,
          direction: 'forward'
        });
      }
    }, err => {
      this.logout()
    })
  }

  private async processUserData(user, showelcome) {

    // check if sqlite set already, if not fetch records
    var _user = user;

    const fcm_token = await this.getFCMToken();
    _user['fcm_token'] = fcm_token;
    this.user_role_id = parseInt(_user['role_id']);
    this.utilityProvider.setKey('user_role_id', this.user_role_id);
    let saveduser = await this.sqlite.setUserInDatabase(user);
    this.menuCtrl.enable(true, 'authenticated');

    if (!saveduser) {
      this.logout();
      return;
    }

    this.userService.setUser(saveduser);
    this.canBeResident = (parseInt(saveduser["can_user_become_resident"]) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    let currentUrl= this.nav.router.url;
    console.log(currentUrl);

    if(currentUrl == '/1/DashboardPage'){
      this.events.publish('dashboard:initialize');
    }else{
      this.nav.setRoot('1/DashboardPage',
      {
        showelcome: showelcome,
        animate: true,
        direction: 'forward'
      }
      );
    }


    

  }

  private async setContacts(user){
    this.users.getContactDatabseOfUser(user).then( v => {
      console.log("import completed", v);
    });
  }


  private async setTokenToServer() {
    const fcm_token = await this.getFCMToken();
    // console.log(fcm_token);
    if(fcm_token){
      this.network.saveFcmToken({ token: fcm_token }).then(dats => {
      }, err => { console.error(err) });
    }
    
  }

  setSuccessPage(params) {
    //// console.log(params);
    this.menuCtrl.enable(true, 'authenticated');
    this.nav.setRoot('SuccessPage', params)
  }

  notificationReceivedalert(data) {
    // // console.log(data.showalert);
    if (data.hasOwnProperty('showalert')) {
      // this.rnotif = true;
      this.utilityProvider.showAlert(data["showalert"]).then(() => {
        if (data['showalert'] == 'Pass scanned successfully') {
          this.nav.setRoot('DashboardPage', {
            animate: true,
            direction: 'backword'
          });
        }
      });
    }



  }

  openPage(page) {


    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    if (page.hasOwnProperty('component')) {

      if(page['component'] == 'VManageVehiclesPage'){
        this.events.publish('dashboard:carselection');
        return;
      }

      if(page['component'] == 'RegistrationPage'){
        this.events.publish('dashboard:updateprofile');
        return;
      }

      this.nav.push(page.component, {revisit: true});
      return;

    }
    if (page.hasOwnProperty('method')) {
        this[page.method]();
    }

    
}


}
