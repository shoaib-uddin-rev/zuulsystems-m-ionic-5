import { ModalService } from './basic/modal.service';
import { NetworkService } from './network.service';
import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { CreateFamilyPageComponent } from '../pages/create-family-page/create-family-page.component';

@Injectable({
  providedIn: 'root'
})
export class PassServiceService {

  constructor(public utility: UtilityService, public sqlite: SqliteService, public network: NetworkService, public modals: ModalService) { }

  getUserEvent(){

    function setDefaultEvent(){

      return {
        event_id: 14,
        event_name: "Event Name"
      };

    }

    return new Promise( async resolve => {

      let event = await this.sqlite.getActiveEventInEvents();
      // console.log(event);
      if(!event){
        resolve(setDefaultEvent());
      }else{
        resolve({
          event_id: event["id"],
          event_name: event["event_name"]
        });
      }


    })

  }

  getFutureDate(date, validity){

    return new Promise( resolve => {
      const dataObj = { date: date, validity: validity }

      this.network.getAddedHourDate(dataObj).then(res => {
        resolve(this.utility.customMDYHMformatDateMDYHM(res['date']));
      })
    })


  }

  getFutureDateISO(date, validity){

    return new Promise( resolve => {

      const dataObj = { date: date, validity: validity }
      console.log(dataObj);
      this.network.getAddedHourDate(dataObj).then(res => {
        console.log(res);
        resolve(res['iso_date']);
      })
    })


  }

  createPass(formdata){

    return new Promise( resolve => {
      console.log(formdata);
      this.network.createNewPass(formdata).then((res: any) =>{
        // console.log(res);
        resolve()
      }, error => {});
    })

  }

  plusHeaderButton(param) {

    // return array of objects of empty array
    return new Promise( resolve => {
      if (param == "S") {
        this.editFamily(null).then( v => {
          if(v){
            resolve([v]);
          }else{
            resolve([]);
          }
        });
        // this.storedcontacts.editFamily(null);
      } else {
        // this.storedcontacts.syncMyContact();
      }
    })

  }

  editFamily(c) {
    return new Promise( async resolve => {
      let _data = { contact_id: c, show_relation: false };
      const data = await this.modals.present(CreateFamilyPageComponent, _data);
      if (data["data"] != "A") {
        // console.log(data);
        resolve(data["contact"])
      }else{
        resolve(null)
      }
    })

  }



}
