import { AlertsService } from './basic/alerts.service';
import { StringsService } from './basic/strings.service';
import { Injectable } from '@angular/core';
import { Contacts } from '@ionic-native/contacts/ngx';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private contacts: Contacts, public strings: StringsService, public alerts: AlertsService) { }

  public getPhoneContacts() {

    return new Promise( async resolve => {
      const contacts = await this.fetchLocalCOntacts();
      resolve(contacts);
    })

  }

  

  fetchLocalCOntacts(){
    return new Promise((resolve, reject) => {
      this.contacts.find(['displayName', 'phoneNumbers', 'emails'], { desiredFields: ['displayName', 'phoneNumbers', 'emails'], filter: "", hasPhoneNumber: true, multiple: true })
        .then(data => {

          var _contacts = [];

          for(var i = 0; i < data.length; i++){
            var count = 0
            var f = {}
            if(!data[i]["displayName"] || data[i]["displayName"] == ""){
              continue;
            }

            if(!data[i]["phoneNumbers"] || data[i]["phoneNumbers"].length <= 0){
              continue;
            }

            f["display_name"] = data[i]["displayName"];
            f["email"] = (data[i]["emails"]) ? data[i]["emails"][0]["value"] : null;

            if ( data[i]["phoneNumbers"] ) {

              //f["phone_number"] = ["4576543265", "5578563534", "6585756373"];
              if ( data[i]["phoneNumbers"].length > 0 ) {

                // var _ccontacts = [];
                data[i]["phoneNumbers"].forEach(element => {

                  // trim phone number for last 10 digits

                  if(!element["value"] || element["value"] == "" ){
                    return;
                  }

                  if(!this.strings.isPhoneNumberValid(element["value"])){
                    return;
                  }

                  f["phone_number"] = element["value"];
                  f["type"] = element["type"];

                  _contacts.push(Object.assign({}, f));
                });



              }


            }

          }

          resolve(_contacts);

        })
        .catch(err => {
          resolve(null);
        })
    })
  }

  public getSinglePhoneContact() {

    return new Promise((resolve) => {
      this.contacts.pickContact()
        .then(data => {
          if (!data.phoneNumbers) { resolve(null); return }
          if (data.phoneNumbers.length > 0) {

            this.selectContactNumberAlertOptions("Select Number", data.phoneNumbers).then(num => {
              resolve(num);
            })

          } else if (data.phoneNumbers.length == 1) {
            var number = (data.phoneNumbers) ? data.phoneNumbers[0]["value"] : null;
            resolve(number);
          }
          else {
            resolve(null);
          }


        })
    })
  }

  public selectContactNumberAlertOptions(title, values) {
    return new Promise(async resolve => {

      var radioOptions = [];
      values.forEach((element, index) => {
        var ph = this.onkeyupFormatPhoneNumberRuntime(element.value);
        radioOptions.push({
          type: 'radio',
          label: ph + '-' + element.type,
          value: ph,
          checked: (index == 0) ? true : false
        });
      });

      const data = await this.alerts.presentRadioSelections(title, '', radioOptions)
      resolve(data);

    })
  }

  getOnlyDigits(phoneNumber) {
    return this.strings.getOnlyDigits(phoneNumber);
  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
    if (phoneNumber == null || phoneNumber == "") { return phoneNumber }

    phoneNumber = this.getOnlyDigits(phoneNumber);
    //phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
    phoneNumber = last ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length) : phoneNumber.substring(0, 10);

    var cleaned = ("" + phoneNumber).replace(/\D/g, '');

    function numDigits(x) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }

    // only keep number and +
    var p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned }
    var p2 = phoneNumber.match(/\d+/g).map(Number);
    var len = numDigits(p2)
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
        return "(" + phoneNumber
      case 3:
        return "(" + phoneNumber + ")"
      case 4:
      case 5:
      case 6:
        var f1 = "(" + phoneNumber.toString().substring(0, 3) + ")"
        var f2 = phoneNumber.toString().substring(len, 3)
        return f1 + " " + f2
      default:
        f1 = "(" + phoneNumber.toString().substring(0, 3) + ")"
        f2 = phoneNumber.toString().substring(6, 3)
        var f3 = phoneNumber.toString().substring(len + 1, 6)
        return f1 + " " + f2 + "-" + f3
    }
  }

}
