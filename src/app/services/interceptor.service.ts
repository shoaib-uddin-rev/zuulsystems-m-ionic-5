import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config/main.config';
import { StorageService } from './basic/storage.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor  {

  api = Config.api;
  api_key = Config.API_KEY
  api_secret = Config.API_SECRET

  constructor(private sqlite: SqliteService, private storage: StorageService, public utility: UtilityService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.callToken()).pipe(
      switchMap(token => {
        const cloneRequest = this.addSecret(req, token );
        return next.handle(cloneRequest);
      })
    );
    
  }

  callToken(){
    return new Promise( resolve => {
      this.sqlite.getCurrentUserAuthorizationToken().then( v => {
        // console.log("token must be here", v);
        resolve(v);
      }).catch( err => {
        console.error(err);
        resolve(null)});
    });
  }

  private addSecret(request: HttpRequest<any>, value: any){
    let v = value ? value : '';
    let clone = request.clone({
      setHeaders : {
        Authorization: 'Bearer ' + v,
        Accept: "application/json",
        'Content-Type': "application/json",
      }
    })
    return clone;
  }
}
