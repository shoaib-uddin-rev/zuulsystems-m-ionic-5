import { Injectable } from '@angular/core';
import { NewPassPageComponent } from '../pages/new-pass-page/new-pass-page.component';
import { VSyncContactsPageComponent } from '../pages/vsync-contacts-page/vsync-contacts-page.component';
import { VSyncContactsStatusPageComponent } from '../pages/vsync-contacts-status-page/vsync-contacts-status-page.component';
import { AlertsService } from './basic/alerts.service';
import { ModalService } from './basic/modal.service';
import { PopoverService } from './basic/popover.service';
import { EventsService } from './events.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class StoredContactsService {

  public search = '';
  public offset = 0;
  public favoffset = 0;
  public syncoffset = 0;

  public is_groupp_loaded = false;
  public phone_contacts: any[] = [];
  public group_contacts: any[] = [];
  public fav_contacts: any[] = [];
  public sync_contacts: any[] = [];

  constructor(public network: NetworkService,
    public atrCtrl: AlertsService,
    public events: EventsService,
    public sqlite: SqliteService,
    public userService: UserService,
    public utilityProvider: UtilityService,
    public modals: ModalService,
    public popoverCtrl: PopoverService,) {

    this.events.subscribe('stored:resetvariables', this.resetAllVariables.bind(this))

  }

  resetAllVariables() {
    console.log('variables reset', Date.now())
    this.search = '';
    this.offset = 0;
    this.favoffset = 0;
    this.syncoffset = 0;

    this.is_groupp_loaded = false;
    this.phone_contacts = [];
    this.group_contacts = [];
    this.fav_contacts = [];
    this.sync_contacts = [];
  }

  getMyContacts(search = null, refresh = false, is_favorite = 0, is_concat = true) {

    return new Promise(async resolve => {

      if (refresh == true || this.offset == 0) {
        // call network for contacts

        resolve(await this.getNetworkContacts(search, is_favorite, is_concat));

      } else {
        // get stored phone contacts
        if (is_favorite == 0) {
          resolve(this.phone_contacts);
        } else {
          resolve(this.fav_contacts);
        }

      }
    })

  }

  getMyGroups(refresh = false, loader = true) {

    return new Promise(async resolve => {
      if (refresh == true) {
        // call network for contacts
        resolve(await this.getContactGroupByUserId(loader));
      } else {
        // get stored phone contacts
        resolve(this.group_contacts);
      }
    })

  }

  async getNetworkContacts(search = null, is_favorite = 0, is_concat = true, loader = false) {


    return new Promise(resolve => {
      let offset = (is_favorite == 0) ? this.offset : this.favoffset;
      if (is_concat == false) { offset = 0 }

      this.sqlite.getContacts(search, offset, is_favorite, loader).then((res: any) => {

        if (is_favorite == 0) {

          if (is_concat) {
            this.offset = res['offset'] as number;
            this.phone_contacts = this.phone_contacts.concat(res['contact_list']);
          } else {
            this.offset = res['offset'] as number;
            this.phone_contacts = res['contact_list'];
          }

          resolve(this.phone_contacts);
        } else {

          if (is_concat) {
            this.favoffset = res['offset'] as number;
            this.fav_contacts = this.fav_contacts.concat(res['contact_list']);
          } else {
            this.favoffset = res['offset'] as number;
            this.fav_contacts = res['contact_list'];
          }

          resolve(this.fav_contacts);
        }
      }, error => {
        if (is_favorite == 0) {
          resolve(this.phone_contacts);
        } else {
          resolve(this.fav_contacts);
        }
      });
    });

  }

  async getSyncNetworkContacts(search = null, is_concat = true, loader = false) {


    return new Promise(resolve => {
      let offset = this.syncoffset;
      if (is_concat == false) { offset = 0 }

      this.sqlite.getSyncContacts(search, offset, loader).then((res: any) => {


        if (is_concat) {
          this.syncoffset = res['offset'] as number;
          this.sync_contacts = this.sync_contacts.concat(res['contact_list']);
        } else {
          this.syncoffset = res['offset'] as number;
          this.sync_contacts = res['contact_list'];
        }

        resolve(this.sync_contacts);

      }, error => {
        resolve(this.sync_contacts);

      });
    });

  }

  async getContactGroupByUserId(loader = true) {
    return new Promise(resolve => {
      this.sqlite.getContactGroupByUserId(loader).then((res: any) => {
        this.group_contacts = res['group'];
        this.is_groupp_loaded = true;
        resolve(this.group_contacts);
      }, error => {
        resolve(this.group_contacts);
      });
    });
  }

  async createSelectedInContactGroupByItem(items) {

    return new Promise(async resolve => {

      if (items.length <= 0) { resolve(null); }

      this.getGroupSelection().then(async id => {
        if (!id) { resolve(null); return; }

        resolve(await this.addContactsToGroup(id, items, false));
      });



    })

  }

  getGroupSelection() {

    return new Promise(async resolve => {

      if (this.is_groupp_loaded == false) {
        await this.getMyGroups(true)
      }



      if (this.group_contacts.length == 0) {
        this.utilityProvider.presentFailureToast('No Group Present');
        resolve(null)
        return;
      }

      var radio_inputs = [];
      this.group_contacts.forEach(item => {
        radio_inputs.push({
          type: 'radio',
          label: item.group_name,
          value: item.id,
          checked: false
        });
      });


      let option = await this.atrCtrl.presentRadioSelections('Select Group', '', radio_inputs);
      resolve(option);


    })
  }

  async addContactsToGroup(group_id, list, loader = true) {
    return new Promise(async resolve => {
      const dataObj = {
        group_id: group_id,
        contact_list: list
      }

      this.network.addContactsToGroup(dataObj, loader).then(async (data: any) => {
        let _list = data['list'];
        await this.sqlite.addContactsToGroup(group_id, _list);
        resolve(_list);
      });



      // .subscribe(data => {
      //   this.phone_contacts.forEach(item => {
      //     item["checked"] = false;
      //   });
      //   this.utilityProvider.presentSuccessToast(data["message"]);
      //   this.getMyGroups(true, loader).then( v => {
      //     resolve();
      //   });

      // }, err => {});
    })

  }

  editFamily(c, is_favorite = 0) {
    return new Promise(async resolve => {
      let id = c ? c.id : null;
      let _data = { contact_id: id, show_relation: false };

      const data = await this.modals.present(
        'CreateFamilyPage',
        _data
      );

      if (data['data'] != 'A') {
        // one contact got edit, only one contact should be updated in sqlite

        let contact = data['contact'];
        if (contact) {
          this.sqlite.setContactListInDatabase([contact]);
          contact.phone_number = contact.formattedPhone;
          // find exact record and update the data
          if (id) {

            var elementPos = this.phone_contacts.map(function (x) { return x.id; }).indexOf(contact.id);
            this.phone_contacts[elementPos] = contact;

            if (parseInt(contact.is_favourite) == 1) {
              var elementPos2 = this.fav_contacts.map(function (x) { return x.id; }).indexOf(contact.id);
              this.fav_contacts[elementPos2] = contact;
            }
          } else {
            contact.new = true;
            this.phone_contacts.unshift(contact);
          }

          resolve(contact);
        }
      }
    })

  }

  async createSelectedInContactFavouritiesByItem(items) {

    var _items = items;

    if (_items.length <= 0) {
      return;
    }

    _items.forEach(async element => {
      await this.sqlite.setContactInFavorites(element)
      element['new'] = true;
      this.fav_contacts.unshift(element)
    });

    this.network.addToFavorites(items).then(async res => {
      //this.utilityProvider.presentSuccessToast(res['message']);
    });

  }

  async deleteContact(item) {

    let flag = await this.utilityProvider.presentConfirm('Agree', 'Disagree', 'Delete Contact?', 'Passes sent to this contact will not be effected');
    if (!flag) return;

    await this.sqlite.removeContactInDatabase(item);
    var elementPos = this.phone_contacts.map(function (x) { return x.id; }).indexOf(item.id);
    this.phone_contacts.splice(elementPos, 1);

    if (parseInt(item.is_favourite) == 1) {
      var elementPos2 = this.fav_contacts.map(function (x) { return x.id; }).indexOf(item.id);
      this.fav_contacts.splice(elementPos2, 1);
    }
    const contactArray = { cs: [item] }

    this.network.deleteContactArray(contactArray).then(async res => {
    });
  }

  async fetchEventsToSendPassTo(item) {

    const response = await this.utilityProvider.presentConfirm('Create Pass', 'Cancel', 'Create a pass', 'Would you like to create a pass and sent it to ' + item.display_name)
    if (response) {
      this.redirectToNewpassPage(item);
    }

  }

  async redirectToNewpassPage(item) {

    const data = await this.modals.present(NewPassPageComponent, {
      item: item,
      rap: false,
      group: false,
      contact: true
    })
  }

  async sendPassRequestToContact(item) {

    let _userid = await this.sqlite.getActiveUserId();







    const data = await this.atrCtrl.presentRadioSelections('Request a pass',
      'Enter some details or reason for pass request as the recipient may not know who you are',
      {
        type: 'textarea',
        name: 'comments',
        placeholder: ''
      });

    if (data) {
      var f = {};
      f['requested_user_id'] = item['user_id'];
      f['sent_user_id'] = _userid;
      f['display_name'] = item['display_name'];
      f['description'] = data.comments;
      f['phone_number'] = item['phone_number'];

      console.log(f);
      this.network.sendRequestForAPass([f]).then((res: any) => {
        if (res['bool'] == false) {
          this.utilityProvider.presentFailureToast(res['message']);
        } else {
          this.utilityProvider.presentSuccessToast(res['message']);
        }

      }, err => {

      });
    }


  }

  async checkIfPhoneSyncAlready() {
    return this.sqlite.checkIfPhoneSyncAlready();
  }
  async SyncContacts(resync = false) {

    return new Promise(async resolve => {
      this.utilityProvider.showLoader();
      this.utilityProvider.getPhoneContacts().then(async contacts => {
        console.log('b');
        // this.sqlite.getContacts(null,0,0,false).then( c => {
        //   console.log(c);
        this.sqlite.setSyncContactListInDatabase(contacts, resync).then(v => {
          this.getSyncNetworkContacts(null, false, false);
          this.utilityProvider.hideLoader();
        });
        // })



      })
      // let contacts = await this.utilityProvider.getPhoneContacts();

    })

  }

  async syncMyContact() {

    const _data = await this.modals.present(VSyncContactsPageComponent);
    const data = _data.data;
    if (data) {
      if (data['data'] != 'A') {
        const data2 = await this.modals.present(VSyncContactsStatusPageComponent, { phone_contacts: data['list'] });
        this.getMyContacts(null, true, 0, false);
      }
    }

  }

  async syncToTempContact() {

    return new Promise(async resolve => {

      const data = await this.modals.present(
        'VSyncContactsPage',
        { fromContacts: true }
      );

      if (data) {
        if (data['data'] != 'A') {
          // add these contacts to temporary contact list
          var imported_list = data['list'];
          var _ids = imported_list;
          _ids = '( ' + _ids.join(',') + ' )';
          await this.sqlite.setTemporaryContactsInDatabase(_ids);
          resolve();

        }
      } else {
        resolve();
      }

    })



  }

  async removeAllTempContacts() {
    return this.sqlite.removeAllContactFromTemporary()
  }

  async syncContactsViaNetworkApi(formdata) {

    return new Promise(resolve => {
      this.network.syncContacts(formdata).then(async (response: any) => {
        this.utilityProvider.presentSuccessToast(response['message']);
        await this.sqlite.setContactListInDatabase(response['list'])
        resolve(response['list']);
        // loading.dismiss();
      }, err => {
        console.error(err['list']);
        resolve(err['list']);
      });
    })


  }

  async removeFavoritesFromContacts(item) {

    await this.sqlite.removeContactInFavorites(item)

    var elementPos = this.fav_contacts.map(function (x) { return x.id; }).indexOf(item.id);
    this.fav_contacts.splice(elementPos, 1);

    this.network.removeFromFavorites([item]).then(data => {
    }, err => { });
  }

  async syncContactsToFavourities() {

    const data = await this.modals.present(
      'VSyncContactsPage',
      { fromContacts: true }
    );

    if (data['data'] != 'A') {
      var imported_list = data['list'];
      let list = await this.getContactsByArrayOfIds(imported_list);
      await this.createSelectedInContactFavouritiesByItem(list);
    }

  }

  async createContactGroup() {
    // CreateGroupPage as modal
    let _data = { phone_contacts: this.phone_contacts };

    const data = await this.modals.present(
      'CreateGroupPage',
      _data
    );

    // add to sqlite
    if (data['data'] != 'A') {
      let _group = data['group'];
      await this.sqlite.setGroupListInDatabase([_group]);
      this.getMyGroups(true);
    }

  }

  editContactGroup(it) {
    // CreateGroupPage as modal
    return new Promise(async resolve => {
      let _data = { phone_contacts: this.phone_contacts, it: it };

      const data = await this.modals.present(
        'CreateGroupPage',
        _data
      );

      if (data['group']) {
        let _group = data['group'];
        await this.sqlite.setGroupListInDatabase([_group]);
        this.getMyGroups(true);
        resolve(data);
      } else {
        resolve(null);
      }

    })

  }

  deleteContactGroup(group) {
    return new Promise(async resolve => {
      await this.sqlite.removeGroupInDatabase(group);
      this.network.deleteContactGroup(group.id).then((res: any) => {
        this.getMyGroups(true).then(v => {
          resolve();
        });
      });
    })

  }

  addContactsToSelectedGroupByItem(group_id, loader = true) {

    return new Promise(async resolve => {
      var self = this;


      const data = await this.modals.present(
        'VSyncContactsPage',
        { fromContacts: true }
      );

      if (data['data'] != 'A') {

        const imported_list = data['list'];

        let list = imported_list.map(x => {
          return {
            contact_id: x
          }
        })

        self.addContactsToGroup(group_id, list, loader).then(v => {
          resolve(v);
        });

      }

    })

  }

  async fetchEventsToSendPassToGroup(group) {

    const flag = await this.utilityProvider.presentConfirm('Create pass', 'Cancel', 'Create a Pass', 'Would you like to create a pass and sent it to all contacts in' + group.group_name);
    if (flag) {

      const data = await this.modals.present(
        'NewPassPage',
        {
          rap: false,
          group: true,
          item: group
        }
      );
    }

  }

  syncContactsFromApi(loader = false) {
    return new Promise(resolve => {

      this.network.getOneTimeContactsData(loader).then(async v => {


        await this.sqlite.setGroupListInDatabase(v['contact_group']);
        await this.sqlite.setGroupCollectionInDatabase(v['contact_collection']);
        await this.sqlite.setContactListInDatabase(v['contact_list']);


        await this.getNetworkContacts(null, 0, false, true)
        await this.getNetworkContacts(null, 1, false, true)
        await this.getContactGroupByUserId(true)
        // this.utilityProvider.hideLoader();


        resolve();

      })

    })

  }

  async returnContactSelection(fromContacts = true, singleSelection = true, isFromRequestPassScreen = false, contact_ids = []) {

    return new Promise(async resolve => {

      const _data = await this.modals.present(
        VSyncContactsPageComponent,
        {
          fromContacts: fromContacts,
          singleSelection: singleSelection,
          isFromRequestPassScreen: isFromRequestPassScreen,
          contact_ids: contact_ids
        }
      );

      const data = _data.data;

      console.log(data);

      if (data['data'] != 'A') {
        // add these contacts to temporary contact list
        var imported_list = data['list'];

        resolve(imported_list);

      } else {
        resolve([]);
      }

    })



  }

  isPnCanSendPass(pn) {
    return new Promise(resolve => {
      this.network.checkIfCellNumberUserCanSentPass(pn).then((res: any) => {
        resolve(true);
      }, err => { resolve(false) })
    })

  }

  async getContactsByArrayOfIds(ids) {
    var _ids = ids;
    _ids = '( ' + _ids.join(',') + ' )';
    console.log(ids)
    let _data = await this.sqlite.getContactsByArrayOfIds(_ids);
    console.log(_data)
    return _data['contact_list'];
  }

}
